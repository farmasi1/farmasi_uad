<?php 

class Captcha extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_barang');
	}
	public function index(){
		$this->form_validation->set_rules('captcha','Captcha','trim|callback_check_captcha|required');
		if($this->form_validation->run()==false){
			$this->load->view('mahasiswa/captcha/index', array('img'=>$this->create_captcha()));
		}else{
			echo "sukses";
		}
	}
	
	public function create_captcha(){
		$option = array(
			'img_path'=>'./captcha1',
			'img_url'=> base_url('captcha'),
			'img_width' =>'150',
			'img_height' =>'30',
			'expiration' => 7200
		);

		$cap = create_captcha($option);
		$image = $cap['image'];
		$this->session->set_userdata('captchaword',$cap['word']);
		return $image;
	}
	public function check_captcha(){
		if($this->input->post('captcha')==$this->session->userdata('captchaword')){
			return true;
		}else{
			$this->form_validation->set_message('check_captcha','salah');
			return false;
		}

	}

}
?>
