<?php 

class Admin extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
    }
	
	public function index(){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $this->load->view('admin1/home');
        }
    }
    public function update_profile(){
        if($this->session->userdata('nama_admin')==null){
            redirect('admin/login');
        }else{
        $id = $this->input->post('xid');
        $data = array(
            'nama_admin' => $this->input->post('xnama'),
            'email_admin' => $this->input->post('xemail')
        );
        $query = $this->m_dsn_login->update_profile($id, $data);
        redirect('admin/admin');
        }

    }

}
 
 
?>