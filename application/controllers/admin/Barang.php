<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct(){
            parent::__construct();
                $this->load->library('session');
                $this->load->library('upload');
                $this->load->model('m_admin_barang');
    }
    public function index()
    {
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $data['barang'] = $this->m_admin_barang->list_barang();
            $this->load->view('admin/barang/v_barang.php', $data);
        }
    }
    public function simpan_barang(){
        $config['upload_path'] = './assets/images/barang/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/barang/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/barang'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $create_at = date('Y-m-d');
                $data = array(
                    'barang_nama' => $this->input->post('xnama'),
                    'barang_keterangan' => $this->input->post('xketerangan'),
                    'barang_harga' => $this->input->post('xharga'),
                    'barang_kondisi' => $this->input->post('xkondisi'),
                    'barang_foto' => $gbr['file_name'],
                    'create_at' => $create_at,
                    'barang_tipe_peminjaman' =>$this->input->post('xjenis')
                );
                $query = $this->m_admin_barang->simpan_barang($data);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di simpan');
                }
                redirect('admin/barang');
            }else{
                redirect('admin/barang');
            }

        }else{
            $create_at = date('Y-m-d');
            $data = array(
                'barang_nama' => $this->input->post('xnama'),
                'barang_keterangan' => $this->input->post('xketerangan'),
                'barang_harga' => $this->input->post('xharga'),
                'barang_kondisi' => $this->input->post('xkondisi'),
                'create_at' => $create_at
            );
            $query = $this->m_admin_barang->simpan_barang_tanpa_image($data);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di simpan');
            }
            redirect('admin/barang');
        }

    }
    public function hapus_galeri($id){
        $this->m_galeri->hapus_galeri($id);
        redirect('adminrt/galeri');
    }
    public function edit_barang(){
        $config['upload_path'] = './assets/images/barang/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/barang/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/barang/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $data = array(
                    'barang_nama' => $this->input->post('xnama'),
                    'barang_keterangan' => $this->input->post('xketerangan'),
                    'barang_harga' => $this->input->post('xharga'),
                    'barang_kondisi' => $this->input->post('xkondisi'),
                    'barang_foto' => $gbr['file_name'],
                    'barang_tipe_peminjaman' => $this->input->post('xjenis')
                );
                $id = $this->input->post('xid');
                $query = $this->m_admin_barang->update_barang($data, $id);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di update');
                }
                redirect('admin/barang');
            }else{
                redirect('admin/barang');
            }

        }else{
            $data = array(
                'barang_nama' => $this->input->post('xnama'),
                'barang_keterangan' => $this->input->post('xketerangan'),
                'barang_harga' => $this->input->post('xharga'),
                'barang_kondisi' => $this->input->post('xkondisi'),
                'barang_tipe_peminjaman' => $this->input->post('xjenis')
            );
            $id = $this->input->post('xid');
            $query = $this->m_admin_barang->update_barang_tanpa_iamge($data, $id);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di edit');
            }
            redirect('admin/barang');
        }
    }
    public function hapus_barang($id){
        $this->m_admin_barang->hapus_barang($id);
        redirect('admin/barang');
    }

    public function barang_perjam(){

    }

}