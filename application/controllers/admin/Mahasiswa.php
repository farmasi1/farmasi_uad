<?php 

class Mahasiswa extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_admin');
    }

    public function index(){
        $data['mahasiswa'] = $this->m_admin->data_mahasiswa();
        $this->load->view('admin/mahasiswa/v_mahasiswa', $data);
    }
    public function detail_mahasiswa($id){
        $data['mahasiswa'] = $this->m_admin->data_detail_mahasiswa($id);
        $this->load->view('admin/mahasiswa/v_detail_mahasiswa', $data);
    }
	

}
 
 
?>