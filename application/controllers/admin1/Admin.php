<?php 

class Admin extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_transaksi');
        $this->load->model('m_mahasiswa');
		$this->load->model('m_admin_barang');
		if($this->session->userdata('admin_nama')==null){
			redirect('admin1/login');
		}
    }
	
	public function index(){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $tahun = date('Y');
            $bulan = date('m');
            $data = array(
                'transaksi_masuk' => $this->m_transaksi->transaksi_masuk(),
                'total_transaksi' => $this->m_transaksi->total_transaksi($tahun,$bulan),
                'total_pendapatan' => $this->m_transaksi->total_pendapatan($tahun,$bulan),
                'total_pengguna' => $this->m_mahasiswa->total_pengguna(),
                'total_barang' => $this->m_admin_barang->total_barang(),
                'total_ruang_masuk' => $this->m_admin_barang->total_ruang_masuk($tahun,$bulan),
                'total_ruangan' => $this->m_admin_barang->total_ruangan(),
            );
            $this->load->view('admin1/home', $data);
        }
    }
    public function update_profile(){
        if($this->session->userdata('nama_admin')==null){
            redirect('admin/login');
        }else{
        $id = $this->input->post('xid');
        $data = array(
            'nama_admin' => $this->input->post('xnama'),
            'email_admin' => $this->input->post('xemail')
        );
        $query = $this->m_dsn_login->update_profile($id, $data);
        redirect('admin/admin');
        }

    }

}
 
 
?>
