<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct(){
            parent::__construct();
            $this->load->library('session');
            $this->load->library('upload');
			$this->load->model('m_admin_barang');
			if($this->session->userdata('admin_nama')==null){
				redirect('admin1/login');
			}
    }
    public function index()
    {
        if($this->session->userdata('admin_nama')==null){
            redirect('admin1/login');
        }else{
            $data['barang'] = $this->m_admin_barang->list_barang();
            $this->load->view('admin1/barang/v_barang.php', $data);
        }
    }
    public function simpan_barang(){
        $config['upload_path'] = './assets/images/barang/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/barang/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/barang'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $create_at = date('Y-m-d');
                $data = array(
                    'barang_nama' => $this->input->post('xnama'),
                    'barang_keterangan' => $this->input->post('xketerangan'),
                    'barang_harga' => $this->input->post('xharga'),
                    'barang_kondisi' => $this->input->post('xkondisi'),
                    'barang_foto' => $gbr['file_name'],
                    'create_at' => $create_at,
                    'barang_tipe_peminjaman' =>$this->input->post('xjenis'),
                    'barang_max_jam' => $this->input->post('xmaxjam'),
                    'barang_min_jam' => $this->input->post('xminjam'),
                    'barang_max_hari' => $this->input->post('xmaxhari'),
                );
                $query = $this->m_admin_barang->simpan_barang($data);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di simpan');
                }
                redirect('admin1/barang');
            }else{
                redirect('admin1/barang');
            }

        }else{
            $create_at = date('Y-m-d');
            $data = array(
                'barang_nama' => $this->input->post('xnama'),
                'barang_keterangan' => $this->input->post('xketerangan'),
                'barang_harga' => $this->input->post('xharga'),
                'barang_kondisi' => $this->input->post('xkondisi'),
                'create_at' => $create_at
            );
            $query = $this->m_admin_barang->simpan_barang_tanpa_image($data);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di simpan');
            }
            redirect('admin1/barang');
        }

    }
    public function hapus_galeri($id){
        $this->m_galeri->hapus_galeri($id);
        redirect('adminrt/galeri');
    }
    public function edit_barang(){
        $config['upload_path'] = './assets/images/barang/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/barang/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/barang/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $data = array(
                    'barang_nama' => $this->input->post('xnama'),
                    'barang_keterangan' => $this->input->post('xketerangan'),
                    'barang_harga' => $this->input->post('xharga'),
                    'barang_kondisi' => $this->input->post('xkondisi'),
                    'barang_foto' => $gbr['file_name'],
                    'barang_tipe_peminjaman' => $this->input->post('xjenis')
                );
                $id = $this->input->post('xid');
                $query = $this->m_admin_barang->update_barang($data, $id);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di update');
                }
                redirect('admin1/barang');
            }else{
                redirect('admin1/barang');
            }

        }else{
            $data = array(
                'barang_nama' => $this->input->post('xnama'),
                'barang_keterangan' => $this->input->post('xketerangan'),
                'barang_harga' => $this->input->post('xharga'),
                'barang_kondisi' => $this->input->post('xkondisi'),
                'barang_tipe_peminjaman' => $this->input->post('xjenis')
            );
            $id = $this->input->post('xid');
            $query = $this->m_admin_barang->update_barang_tanpa_iamge($data, $id);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di edit');
            }
            redirect('admin1/barang');
        }
	}
	public function edit_barang_1(){
        $config['upload_path'] = './assets/images/barang/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/barang/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 300;
                $config['height']= 300;
                $config['new_image']= './assets/images/barang/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$jenis_barang = $this->input->post('xjenis');
				if($jenis_barang == 1){
					$data = array(
						'barang_nama' => $this->input->post('xnama'),
						'barang_keterangan' => $this->input->post('xketerangan'),
						'barang_harga' => $this->input->post('xharga'),
						'barang_kondisi' => $this->input->post('xkondisi'),
						'barang_tipe_peminjaman' => $jenis_barang,
						'barang_min_jam' => $this->input->post('xminjam'),
						'barang_max_jam' => $this->input->post('xmaxjam'),
						'barang_foto' => $gbr['file_name'],
					);
				}elseif($jenis_barang == 2){
					$data = array(
						'barang_nama' => $this->input->post('xnama'),
						'barang_keterangan' => $this->input->post('xketerangan'),
						'barang_harga' => $this->input->post('xharga'),
						'barang_kondisi' => $this->input->post('xkondisi'),
						'barang_tipe_peminjaman' => $jenis_barang,
						'barang_max_hari' => $this->input->post('xmaxhari'),
						'barang_foto' => $gbr['file_name'],
					);
				}
				
                $id = $this->input->post('xid');
                $query = $this->m_admin_barang->update_barang($data, $id);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di update');
                }
                redirect('admin1/barang');
            }else{
                redirect('admin1/barang');
            }

        }else{
			$jenis_barang = $this->input->post('xjenis');
			if($jenis_barang == 1){
				$data = array(
					'barang_nama' => $this->input->post('xnama'),
					'barang_keterangan' => $this->input->post('xketerangan'),
					'barang_harga' => $this->input->post('xharga'),
					'barang_kondisi' => $this->input->post('xkondisi'),
					'barang_tipe_peminjaman' => $jenis_barang,
					'barang_min_jam' => $this->input->post('xminjam'),
					'barang_max_jam' => $this->input->post('xmaxjam'),
				);
			}elseif($jenis_barang == 2){
				$data = array(
					'barang_nama' => $this->input->post('xnama'),
					'barang_keterangan' => $this->input->post('xketerangan'),
					'barang_harga' => $this->input->post('xharga'),
					'barang_kondisi' => $this->input->post('xkondisi'),
					'barang_tipe_peminjaman' => $jenis_barang,
					'barang_max_hari' => $this->input->post('xmaxhari'),
				);
			}

            $id = $this->input->post('xid');
            $query = $this->m_admin_barang->update_barang_tanpa_iamge($data, $id);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di edit');
            }
            redirect('admin1/barang');
        }
    }
    public function hapus_barang($id){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
        $this->m_admin_barang->hapus_barang($id);
        redirect('admin1/barang');
        }
    }

    public function laporan_perbulan(){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin1/login');
        }else{
        $id = $this->input->post('xidbarang');
        $tahun = $this->input->post('xtahun');
        $bulang = $this->input->post('xbulan');
        $data = array(
            'nama_barang' => $this->m_admin_barang->nama_barang($id),
            'harga_barang' => $this->m_admin_barang->harga_barang($id),
            'jenis_barnag'=> $this->m_admin_barang->jenis_barang($id),
            'tanggal_sewa' => $this->m_admin_barang->list_tanggal_sewa($id, $tahun, $bulang),
            'total_harga' => $this->m_admin_barang->total_harga($id, $tahun, $bulang),
            'list_durasi_tersewa' => $this->m_admin_barang->list_durasi_tersewa($id, $tahun, $bulang),
            'bulan' => $bulang,
            'tahun' => $tahun,
            'id'=>$id,
        );
        // $data = $this->m_admin_barang->laporan_perbulan($id, $tahun, $bulang);
        $this->load->view('admin1/barang/v_laporan_bulan.php', $data);
        }
    }

    public function laporan_pertahun(){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin1/login');
        }else{
            $id = $this->input->post('xidbarang');
            $tahun = $this->input->post('xtahun');
            $data = array(
                'nama_barang' => $this->m_admin_barang->nama_barang($id),
                'harga_barang' => $this->m_admin_barang->harga_barang($id),
                'jenis_barnag'=> $this->m_admin_barang->jenis_barang($id),
                'tanggal_sewa' => $this->m_admin_barang->list_tanggal_sewa_tahun($id, $tahun),
                'total_harga' => $this->m_admin_barang->total_harga_tahun($id, $tahun),
                'list_durasi_tersewa' => $this->m_admin_barang->list_durasi_tersewa_tahun($id, $tahun),
                'tahun' => $tahun,
                'id'=>$id,
            );
            // $data = $this->m_admin_barang->laporan_perbulan($id, $tahun, $bulang);
            $this->load->view('admin1/barang/v_laporan_tahun.php', $data);
        }

	}
	public function update_barang($id){
		$data = array(
			'barang' => $this->m_admin_barang->detail_barang($id),
		);

		$this->load->view('admin1/barang/v_update_barang.php', $data);
	}

}
