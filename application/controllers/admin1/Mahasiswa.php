<?php 

class Mahasiswa extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_admin');
		$this->load->model('m_mahasiswa');
		if($this->session->userdata('admin_nama')==null){
			redirect('admin1/login');
		}
    }

    public function index(){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $data['mahasiswa'] = $this->m_admin->data_mahasiswa();
            $this->load->view('admin1/mahasiswa/v_mahasiswa', $data);
        }
    }
    public function detail_mahasiswa($id){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $nim = $this->m_mahasiswa->ambil_nim($id);
            $data = array(
                'mahasiswa' => $this->m_admin->data_detail_mahasiswa($id),
				'verifikasi' => $this->m_mahasiswa->status_verifikasi_mahasiswa($id),
				'status_pengguna' => $this->m_mahasiswa->status_pengguna($id),
                'status' => $this->m_mahasiswa->status_mahasiswa($id),
                'id' => $id,
                'riwayat'=>$this->m_mahasiswa->riwayat_peminjaman($nim),
                'total_penyewaan'=>$this->m_mahasiswa->total_penyewaan($nim),
                'total_biaya'=>$this->m_mahasiswa->total_biaya($nim),
            );
            $this->load->view('admin1/mahasiswa/v_detail_mahasiswa', $data);
        }
    }

    public function verifikasi_mahasiswa(){
        
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $data = array(
            'mahasiswa_verifikasi'=>2,
        );
        $id = $this->input->post('xid');
            $this->m_mahasiswa->update_verifikasi($id, $data);
            redirect("admin1/mahasiswa/detail_mahasiswa/$id");
        }
    }
    public function tolak_verifikasi($id){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $data = array(
                'mahasiswa_verifikasi'=>0,
            );
            $this->m_mahasiswa->tolak_verifikasi($id, $data);
            redirect("admin1/mahasiswa/detail_mahasiswa/$id");
        }
	}
	public function non_aktifkan($id){
		$data = array(
			'mahasiswa_status' => 1,
		);
		$this->m_mahasiswa->non_aktifkan($id, $data);
		redirect("admin1/mahasiswa/detail_mahasiswa/$id");
	}

	public function re_aktifkan($id){
		$data = array(
			'mahasiswa_status' => 0,
		);
		$this->m_mahasiswa->re_aktifkan($id, $data);
		redirect("admin1/mahasiswa/detail_mahasiswa/$id");
	}
	

}
 
 
?>
