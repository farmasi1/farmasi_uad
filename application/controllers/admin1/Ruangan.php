<?php 

class Ruangan extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_transaksi');
        $this->load->model('m_mahasiswa');
		$this->load->model('m_admin_barang');
		if($this->session->userdata('admin_nama')==null){
			redirect('admin1/login');
		}
    }
	
	public function index(){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $data = array(
                'ruangan'=> $this->m_admin_barang->ruangan()
            );
            $this->load->view('admin1/ruangan/v_ruangan', $data);
        }
    }
    
    public function simpan_ruangan(){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $data = array(
                'ruang_nama'=> $this->input->post('xnama'),
                'create_at' => date('Y-m-d'),
            );
            $this->m_admin_barang->simpan_ruangan($data);
            redirect('admin1/ruangan');
        }
    }
    public function hapus_ruangan($id){
        if($this->session->userdata('admin_nama')==null){
            redirect('admin/login');
        }else{
            $data = array(
                'ruang_id' =>$id,
            );
            $this->m_admin_barang->hapus_ruangan($data);
            redirect('admin1/ruangan');
        }
    }
    public function edit_ruangan(){
        $id = $this->input->post('xid');
        $data = array(
            'ruang_nama'=>$this->input->post('xnama'),
        );
        $this->m_admin_barang->edit_ruangan($data, $id);
        redirect('admin1/ruangan');
    }

}
 
 
?>
