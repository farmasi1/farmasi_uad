<?php

class Transaksi extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
		$this->load->model('m_transaksi');
		if($this->session->userdata('admin_nama')==null){
			redirect('admin1/login');
		}
    }

    public function index(){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'transaksi' => $this->m_transaksi->tranaksi_masuk()
            );
            $this->load->view('admin1/transaksi/v_transaksi.php', $data);
        }
    }
    public function tolak_penyewaan($kode_penyewaan){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'status_transaksi'=>3
            );
            $this->m_transaksi->tolak_penyewaan($kode_penyewaan, $data);
            redirect('admin1/transaksi');
        }
    }
    public function transaksi_diterima(){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'transaksi' => $this->m_transaksi->transaksi_diterima()
            );
            $this->load->view('admin1/transaksi/v_transaksi_diterima.php', $data);
        }
    }
    public function terima_penyewaan($kode_penyewaan){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'status_transaksi'=>2
            );
            $this->m_transaksi->terima_penyewaan($kode_penyewaan, $data);
            redirect('admin1/transaksi');
        }
    }
    public function transaksi_ditolak(){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'transaksi' => $this->m_transaksi->transaksi_ditolak()
            );
            $this->load->view('admin1/transaksi/v_transaksi_ditolak.php', $data);
        }
    }
    public function transaksi_masuk_ruangan(){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'transaksi_ruangan' => $this->m_transaksi->tranaksi_masuk_ruangan()
            );
            $this->load->view('admin1/transaksi/ruangan/v_transaksi.php', $data);
        }
    }

    public function terima_penyewaan_ruangan($kode){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'status_transaksi' => 2,
            );
            $this->m_transaksi->terima_penyewaan_ruangan($kode, $data);
            redirect('admin1/transaksi/transaksi_masuk_ruangan');
        }
    }
    public function tolak_penyewaan_ruangan($kode){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'status_transaksi' => 3,
            );
            $this->m_transaksi->tolak_penyewaan_ruangan($kode, $data);
            redirect('admin1/transaksi/transaksi_masuk_ruangan');
        }
    }
    public function transaksi_diterima_ruangan(){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'transaksi_ruangan' => $this->m_transaksi->transaksi_diterima_ruangan()
            );
            $this->load->view('admin1/transaksi/ruangan/v_transaksi_diterima.php', $data);
        }
    }
    public function transaksi_ditolak_ruangan(){
        if ($this->session->userdata('admin_email')==null){
            $this->load->view('admin/login/login');
        }else{
            $data = array(
                'transaksi_ruangan' => $this->m_transaksi->transaksi_ditolak_ruangan()
            );
            $this->load->view('admin1/transaksi/ruangan/v_transaksi_ditolak.php', $data);
        }
    }

}

?>
