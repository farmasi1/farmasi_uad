<?php 

class Barang extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_barang');

    }
	
	public function index(){
        $data['barang'] = $this->m_mhs_barang->list_barang_limit();
        $this->load->view('mahasiswa/barang/v_barang.php', $data);
    }
    public function detail_barang($id){
        $data = array(
            'barang'=>$this->m_mhs_barang->detail_barang($id),
            'tanggal'=>$this->m_mhs_barang->ambil_tanggal($id),
            'barang_id'=>$id,
            'status_barang'=> $this->m_mhs_barang->cek_status_barang($id),
            'jam'=>$this->m_mhs_barang->list_jam(),
            'max_hari'=> $this->m_mhs_barang->max_hari($id),
        );
        $this->load->view('mahasiswa/barang/v_detail_barang', $data);
    }
    
    public function cek_jadwal_peminjaman_jam(){
        $id = $this->input->post('xbarangid');
        $tanggal = $this->input->post('xtanggal');
        $waktu = $this->m_mhs_barang->durasi_tersewa($id, $tanggal);
        $a = array();
        foreach($waktu as $v){
            $loop = explode(",",$v->durasi_jam);
            foreach ($loop as $key => $value) {
                $a[] = $value;
            }
        }
        // print_r($a);
        $jam_terpakai = array();
        for($i = 1; $i < 24; $i++){
            if(in_array($i,$a)){
                $jam_terpakai[$i] = $i; 
            } else {
                $jam_terpakai[$i] = 0;
            }
        }

        $data = array(
            'barang'=>$this->m_mhs_barang->detail_barang($id),
            'tanggal'=>$this->m_mhs_barang->ambil_tanggal($id),
            'barang_id'=>$id,
            'status_barang'=> $this->m_mhs_barang->cek_status_barang($id),
            'jam'=>$this->m_mhs_barang->list_jam(),
            'jam_terpakai'=>$jam_terpakai,
            'tanggal_sewa'=>$tanggal,
            'min_jam'=>$this->m_mhs_barang->min_jam($id),
            'max_jam'=>$this->m_mhs_barang->max_jam($id),
        );
        $this->load->view('mahasiswa/barang/v_detail_barang_cek', $data);
    }
    public function cari_barang(){
        $search = $this->input->post('xsearch');
        $data = array(
            'caribarang'=> $this->m_mhs_barang->cari_barang($search),
        );
        $this->load->view('mahasiswa/barang/v_cari_barang.php', $data);
    }


}
?>
