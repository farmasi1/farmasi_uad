<?php 
class Home extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_barang');
    }
	
	public function index(){
        $data['barang'] = $this->m_mhs_barang->list_barang();
        $this->load->view('mahasiswa/v_home.php', $data);
    }

}
?>
