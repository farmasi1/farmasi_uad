<?php

class Login extends CI_Controller {

    public function __construct(){

            parent::__construct();
                $this->load->helper('url');
                $this->load->library('Session');
                $this->load->model('m_mhs_login');
                $this->load->library('upload');


    }

    public function index(){
        $this->load->view("mahasiswa/login/v_daftar.php");
    }

    public function daftar_akun(){
        $nama = $this->input->post('xnama');
        $email = $this->input->post('xemail');
        $nim = $this->input->post('xnim');
        $jk = $this->input->post('xjk');
        $status = $this->input->post('xstatus');
        $ktp = $this->input->post('xktp');
        $nohp = $this->input->post('xnohp');
        $password = md5($this->input->post('xpassword'));
        
        $create_at = date('Y-m-d');
            
        $cek_email=$this->m_mhs_login->cek_email($email);
        if($cek_email){
            $config['upload_path'] = './assets/images/profile/'; //path folder
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
            $this->upload->initialize($config);
            if(!empty($_FILES['filefoto']['name']))
            {
                if ($this->upload->do_upload('filefoto'))
                {
                    $gbr = $this->upload->data();

                    $config['image_library']='gd2';
                    $config['source_image']='./assets/images/profile/'.$gbr['file_name'];
                    $config['create_thumb']= FALSE;
                    $config['maintain_ratio']= FALSE;
                    $config['quality']= '50%';
                    $config['width']= 300;
                    $config['height']= 300;
                    $config['new_image']= './assets/images/profile'.$gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $create_at = date('Y-m-d');
                    $data = array(
                        'mahasiswa_nim'=>$nim,
                        'mahasiswa_jenis'=>$status,
                        'mahasiswa_nama'=>$nama,
                        'mahasiswa_email'=>$email,
                        'mahasiswa_nohp'=>$nohp,
                        'mahasiswa_password'=>$password,
                        'mahasiswa_ktp'=>$ktp,
                        'mahasiswa_jk'=>$jk,
                        'create_at'=>$create_at,
                        'mahasiswa_foto'=>$gbr['file_name'],
                        'mahasiswa_instansi'=>$this->input->post('xinstansi')
                    );
                    $this->m_mhs_login->daftar_mahasiswa($data);
                }
            }
            // $id = $this->m_mhs_login->ambil_id($email);
            // $this->session->set_flashdata('flash_msg', 'Silahkan verifikasi email anda');
            //set up email
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'qwertyzxcvbn081@gmail.com', // change it to yours
                    'smtp_pass' => 'yangbukaemailiniselaingemaantikaharam01', // change it to yours
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1',
                    'wordwrap' => TRUE
                );
            $message = 	"
                        <html>
                        <head>
                            <title>Email verifikasi</title>
                        </head>
                        <body>
                            <p>Klik Disini untuk aktivasi akun</p>
                            <h4><a href='".base_url()."mahasiswa/login/activate/".$email."'>Aktifkan Akun</a></h4>
                        </body>
                        </html>
                        ";
            
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($config['smtp_user']);
            $this->email->to($email);
            $this->email->subject('Signup Verification Email');
            $this->email->message($message);
            if($this->email->send()){
                $this->session->set_flashdata('flash_msg','Kode verifikasi terkirim ke email ');
                }
                else{
                    $this->session->set_flashdata('flash_msg', $this->email->print_debugger());
                }
            redirect('mahasiswa/home');
        }
        else{
            $this->session->set_flashdata('flash_msg', 'Email Sudah Terdaftar');
            redirect('mahasiswa/home');
        }   

    }

    public function login_view(){
        $this->load->view("customer/login/v_login_customer");

    }

    function masuk_mahasiswa(){
            $email = $this->input->post('xemail');
            $password = md5($this->input->post('xpassword'));
            $data=$this->m_mhs_login->login_mhs($email,$password);
            if($data)
            {
                $status = $this->m_mhs_login->ambil_status($email);
                if($status != 1){
                    $this->session->set_flashdata('flash_msg', 'Akun Belum teraktifasi');
                    redirect('mahasiswa/home');
                }else{
                    $this->session->set_userdata('mahasiswa_id',$data['mahasiswa_id']);
                    $this->session->set_userdata('mahasiswa_nim',$data['mahasiswa_nim']);
                    $this->session->set_userdata('mahasiswa_email',$data['mahasiswa_email']);
                    $this->session->set_userdata('mahasiswa_nama',$data['mahasiswa_nama']);
                    $this->session->set_userdata('mahasiswa_jenis',$data['mahasiswa_jenis']);
                // <?php echo $this->session->userdata('email_mahasiswa'); buat echo doang biar inget
                    redirect('mahasiswa/home');
                }
            }
            else{
                $this->session->set_flashdata('flash_msg', 'Kombinasi email dan password salah.!');
                redirect('mahasiswa/home');
            }


    }

    function user_profile(){

        $this->load->view('admin/user/v_user_profile.php');
        }

    public function keluar(){
        $this->session->sess_destroy();
        redirect('mahasiswa/home');
    }

    public function activate($email){	
		// $email =  $this->uri->segment(4);
		// $code = $this->uri->segment(5);	
		//fetch user details
			$query = $this->m_mhs_login->activin($email);
			if($query){
				$this->session->set_flashdata('flash_msg', 'Akun berhasil di aktifkan, silahkan Masuk.!');
			}
			else{
				$this->session->set_flashdata('flash_msg', 'Aktifasi gagal, silahkan daftar ulang');
			}
		redirect('mahasiswa/home');
    }
    
    // public function lupa_password(){
    //     $this->load->view('customer/login/v_reset_password');
    // }

    public function lupa_password(){
        $email = $this->input->post('xemail');
        $email_check=$this->m_mhs_login->email_checkpass($email);
        if($email_check != null){

            $id = $this->m_mhs_login->ambil_id($email);
            //set up email
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'qwertyzxcvbn081@gmail.com', // change it to yours
                    'smtp_pass' => 'yangbukaemailiniselaingemaantikaharam01', // change it to yours
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1',
                    'wordwrap' => TRUE
            );
            $message = 	"
                        <html>
                        <head>
                            <title>Reset Password SISELA FARMASI UAD</title>
                        </head>
                        <body>
                            <h6>Reset Password</h6>
                            <p>Klik Disini untuk reset password</p>
                            <h4><a href='".base_url()."mahasiswa/login/update_password/".$id."'>reset password akun SISELA FARMASI UAD</a></h4>
                        </body>
                        </html>
                        ";
            
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($config['smtp_user']);
            $this->email->to($email);
            $this->email->subject('Reset password SISELA FARMASI UAD');
            $this->email->message($message);
            if($this->email->send()){
                $this->session->set_flashdata('flash_msg','Link ubah password terkirim ke email anda');
                }
                else{
                    $this->session->set_flashdata('emaileror', $this->email->print_debugger());
                }
            redirect('mahasiswa/home');
        }
        else{
                $this->session->set_flashdata('flash_msg','Email Tidak Terdaftar');
                redirect('mahasiswa/home');
        }
    }

    function update_password($id){
        $data['email'] = $this->m_mhs_login->ambil_email($id);
        $data['id'] = $id;
        $this->load->view('mahasiswa/login/v_ubah_password.php', $data);
    }

    function password_baru(){
        $id = $this->input->post('xid');
        $password = md5($this->input->post('xpassword'));

        $query = $this->m_mhs_login->passworbaru($id, $password);
        if($query){
            $this->session->set_flashdata('flash_msg', 'Ubah Password Berhasil');
            redirect('mahasiswa/login');
        }else{
            $this->session->set_flashdata('flash_msg', 'Ganti Password Gagal');
            redirect('mahasiswa/login');
        }
    }
    public function simpan_password_baru(){
        $id = $this->input->post('xid');
        $data = array(
            'mahasiswa_password'=> md5($this->input->post('xpassword')),
        );
        $query = $this->m_mhs_login->simpan_password_baru($id, $data);
        if($query){
            $this->session->set_flashdata('flash_msg','Berhasil Ubah Password');
        }
        redirect('mahasiswa/home');
    }

}

?>
