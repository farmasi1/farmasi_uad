<?php 

class Profile extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_login');
        $this->load->model('m_mahasiswa');
        $this->load->library('upload');
    }
	
	public function index(){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('admin/login');
        }else{
            $nim = $this->session->userdata('mahasiswa_nim');
            $data = array(
                'mahasiswa' => $this->m_mhs_login->indentitas_mahasiswa($nim),
                'nim'=>$nim,
                'verifikasi'=>$this->m_mahasiswa->status_verifikasi($nim)
            );
            $this->load->view('mahasiswa/profile/v_profile.php', $data);
        }
    }

    public function update_profile(){
        if($this->session->userdata('mahasiswa_id')==null){
            redirect('mahasiswa/login');
        }else{
            $password = md5($this->input->post('xpassword'));
            $id = $this->input->post('xid');
            $pass = $this->m_mhs_login->ambil_password($id);
            if($pass == $password){
                $data = array(
                    'mahasiswa_nim'=>$this->input->post('xnim'),
                    'mahasiswa_nama'=>$this->input->post('xnama'),
                    'mahasiswa_nohp'=>$this->input->post('xnohp'),
                    'mahasiswa_ktp'=>$this->input->post('xktp'),
                    'mahasiswa_alamat'=>$this->input->post('xalamat'),
                    'mahasiswa_instansi'=>$this->input->post('xinstansi')
                );
                    $this->m_mhs_login->update_profile($data, $id);
                    $this->session->set_flashdata('pesan','Update Profile Berhasil');
                    redirect('mahasiswa/profile');
            }else{
                $this->session->set_flashdata('pesan','Password Anda Salah');
                redirect('mahasiswa/profile');
            }
        }
    }

    public function verifikasi(){
        if($this->session->userdata('mahasiswa_id')==null){
            redirect('mahasiswa/login');
        }else{
            $id = $this->input->post('xid');
            $jenismahasiswa = $this->m_mahasiswa->cek_jenis($id);
            
            // ini buat mahasiswa
            if($jenismahasiswa == 1){
                $config['upload_path'] = './assets/file/uad/'; //path folder
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan
                $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
                $this->upload->initialize($config);
                // ini buat file skripsi    
                if(!empty($_FILES['xskripsi']['name'])){
                    if ($this->upload->do_upload('xskripsi'))
                    {
                        $fl = $this->upload->data();
                        $config['new_file']= './assets/file/uad/'.$fl['file_name'];
                        $this->load->library('image_lib', $config);
                        $file=$fl['file_name'];
                        $data = array(
                            'file_skripsi' => $file
                        );
                        $query = $this->m_mahasiswa->update_skripsi($id,$data);
                    }
                }
                // ini buat file skripsi
                // ini buat file kedua    
                if(!empty($_FILES['xbukti']['name'])){
                    if ($this->upload->do_upload('xbukti'))
                    {
                        $fl = $this->upload->data();
                        $config['new_file']= './assets/file/uad/'.$fl['file_name'];
                        $this->load->library('image_lib', $config);
                        $file=$fl['file_name'];
                        $data = array(
                            'file_bukti_transfer' => $file
                        );
                        $query = $this->m_mahasiswa->update_bukti($id,$data);
                    }
                }
                $data = array(
                    'mahasiswa_verifikasi'=>1
                );
                $query = $this->m_mahasiswa->update_verifikasi($id, $data);
                // ini buat file kedua
            }else{
                $config['upload_path'] = './assets/file/luar/'; //path folder
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan
                $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
                $this->upload->initialize($config);
                // ini buat file skripsi    
                if(!empty($_FILES['xizin']['name'])){
                    if ($this->upload->do_upload('xizin'))
                    {
                        $fl = $this->upload->data();
                        $config['new_file']= './assets/file/luar/'.$fl['file_name'];
                        $this->load->library('image_lib', $config);
                        $file=$fl['file_name'];
                        $data = array(
                            'file_surat_izin_lab' => $file
                        );
                        $query = $this->m_mahasiswa->update_izin($id,$data);
                    }
                }
                // ini buat file skripsi
                // ini buat file kedua    
                if(!empty($_FILES['xbukti']['name'])){
                    if ($this->upload->do_upload('xbukti'))
                    {
                        $fl = $this->upload->data();
                        $config['new_file']= './assets/file/luar/'.$fl['file_name'];
                        $this->load->library('image_lib', $config);
                        $file=$fl['file_name'];
                        $data = array(
                            'file_bukti_transfer' => $file
                        );
                        $query = $this->m_mahasiswa->update_bukti($id,$data);
                    }
                }
                // ini buat file kedua    
                // ini buat file kedua    
                if(!empty($_FILES['xpassport']['name'])){
                    if ($this->upload->do_upload('xpassport'))
                    {
                        $fl = $this->upload->data();
                        $config['new_file']= './assets/file/luar/'.$fl['file_name'];
                        $this->load->library('image_lib', $config);
                        $file=$fl['file_name'];
                        $data = array(
                            'file_ktp' => $file
                        );
                        $query = $this->m_mahasiswa->update_ktp($id,$data);
                    }
                }
                $data = array(
                    'mahasiswa_verifikasi'=>1
                );
                $query = $this->m_mahasiswa->update_verifikasi($id, $data);
                // ini buat file kedua
            }
            // ini buat mahasiswa
            redirect('mahasiswa/profile');
        }

    }

}
?>