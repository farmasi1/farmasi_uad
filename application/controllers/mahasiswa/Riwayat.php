<?php 
class Riwayat extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_barang');
        $this->load->model('m_mhs_login');
        $this->load->model('m_mahasiswa');
    }
	
	public function index(){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahassiwa/home');
        }else{
            $nim = $this->session->userdata('mahasiswa_nim');
            $data = array(
                'riwayat'=>$this->m_mahasiswa->riwayat_peminjaman($nim),
                'riwayat_ruangan'=>$this->m_mahasiswa->riwayat_peminjaman_ruangan($nim),
                'nim' => $nim,
            );
            $this->load->view('mahasiswa/riwayat/v_riwayat.php', $data);
        }
    }
    public function cetak_riwayat($nim){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahassiwa/home');
        }else{
            $data = array(
                'biodata'=>$this->m_mhs_login->biodata($nim),
                'riwayat_jam'=>$this->m_mahasiswa->riwayat_sewa_mahasiswa_jam($nim),
                'riwayat_hari'=>$this->m_mahasiswa->riwayat_sewa_mahasiswa_hari($nim),
                'total_biaya'=>$this->m_mahasiswa->total_biaya($nim),
            );
            $this->load->view('mahasiswa/riwayat/v_cetak_riwayat', $data);
        }
    }

}
?>