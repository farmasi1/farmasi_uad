<?php 

class Ruangan extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_ruangan');
        $this->load->model('m_mhs_barang');

    }
	
	public function index(){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahassiwa/home');
        }else{
            $data = array(
                'ruangan'=>$this->m_mhs_ruangan->data_ruangan(),
                'jam'=>$this->m_mhs_barang->list_jam()
            );
            $this->load->view('mahasiswa/ruangan/v_tampil_ruangan', $data);
        }
    }
    public function cek_jadwal_peminjaman_ruangan(){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahassiwa/home');
        }else{
            $id = $this->input->post('xidruang');
            $tanggal = $this->input->post('xtanggal');
            $waktu = $this->m_mhs_ruangan->durasi_tersewa($id, $tanggal);
            $a = array();
            foreach($waktu as $v){
                $loop = explode(",",$v->durasi_jam);
                foreach ($loop as $key => $value) {
                    $a[] = $value;
                }
            }
            // print_r($a);
            $jam_terpakai = array();
            for($i = 1; $i < 24; $i++){
                if(in_array($i,$a)){
                    $jam_terpakai[$i] = $i; 
                } else {
                    $jam_terpakai[$i] = 0;
                }
            }
            $data = array(
                'ruang_id' => $this->input->post('xidruang'),
                'tanggal' => $this->input->post('xtanggal'),
                'ruang_nama' => $this->m_mhs_ruangan->nama_ruang($this->input->post('xidruang')),
                'ruangan'=>$this->m_mhs_ruangan->data_ruangan(),
                'jam'=>$this->m_mhs_barang->list_jam(),
                'jam_terpakai'=>$jam_terpakai,
            );
            $this->load->view('mahasiswa/ruangan/v_tampil_ruangan_cek', $data);
        }
    }
    public function cek_jadwal_peminjaman_ruangan_terpakai(){
        $id = $this->input->post('xbarangid');
        $tanggal = $this->input->post('xtanggal');
        $waktu = $this->m_mhs_barang->durasi_tersewa($id, $tanggal);
        $a = array();
        foreach($waktu as $v){
            $loop = explode(",",$v->durasi_jam);
            foreach ($loop as $key => $value) {
                $a[] = $value;
            }
        }
        // print_r($a);
        $jam_terpakai = array();
        for($i = 1; $i < 24; $i++){
            if(in_array($i,$a)){
                $jam_terpakai[$i] = $i; 
            } else {
                $jam_terpakai[$i] = 0;
            }
        }

        $data = array(
            'barang'=>$this->m_mhs_barang->detail_barang($id),
            'tanggal'=>$this->m_mhs_barang->ambil_tanggal($id),
            'barang_id'=>$id,
            'status_barang'=> $this->m_mhs_barang->cek_status_barang($id),
            'jam'=>$this->m_mhs_barang->list_jam(),
            'jam_terpakai'=>$jam_terpakai,
            'tanggal_sewa'=>$tanggal,
        );
        $this->load->view('mahasiswa/barang/v_detail_barang_cek', $data);
    }
    
  
}
?>
