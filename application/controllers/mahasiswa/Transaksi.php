<?php 

class Transaksi extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_transaksi');
        $this->load->model('m_admin_barang');
        $this->load->model('m_mhs_login');

    }
	
	// public function index(){
    //     if($this->session->userdata('mahasiswa_nim')==null){
    //         redirect('admin/login');
    //     }else{
    //         $this->load->view('mahasiswa/profile/v_profile.php');
    //     }
    // }
    public function simpan_transaksi_perhari(){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahasiswa/login');
        }else{
            $nim = $this->session->userdata('mahasiswa_nim');
			$cek_status = $this->m_mhs_login->cek_status_penyewa($nim);
			$mahasiswa_status = $this->m_mhs_login->status_mahasiswa($nim); // ini untuk pengecekan apakah sudah melebihi waktu peminjaman atau belum
			
			if($mahasiswa_status == 1){
                $this->session->set_flashdata('pesan','Gagal Melakukan Transaksi.! Anda Sudah Di Non-Akftifkan');
				redirect('mahasiswa/profile');
			}

            if($cek_status == 2){
				$lama = $this->input->post("xlama");
				$tanggal = $this->input->post("xtanggal");
				$waktu = array(); // ini array buat penampung tanggal nanti
				for($i =0; $i<$lama; $i++){
				$tanggal_pinjam = date('Y-m-d', strtotime('+'.$i.'days', strtotime($tanggal))); // ini buat ambil tanggal selanjutnya
				array_push($waktu, $tanggal_pinjam); // simpan rentang waktu ke dalam array
				}
				$durasi = implode(",",$waktu); // array tanggal ubah ke string
				$rest = substr(sha1($this->input->post('xtujuan').$durasi.$this->input->post('xbarangid')), 0, 6);  // buat kode pertama
				$kodefix = $this->input->post('xnim').$rest;
				// print_r($waktu);
				$tanggal_tersewa = $this->m_admin_barang->tanggal_tersewa(); // ambil tanggal tersewa
				$a = array(); // ubah tanggal result ke array
				foreach($tanggal_tersewa as $v){
					$loop = explode(",",$v->tgl_sewa);
					foreach ($loop as $key => $value) {
						$a[] = $value;
					}
				}
				// print_r($a);
				$batas = count($waktu); // menghitung banyak tanggal yang di ambil dari pilihan
				for($i =0; $i<$batas; $i++){ 
					if (in_array($waktu[$i], $a)){
						$this->session->set_flashdata('pesan','Tanggal Sudah Terboking');
						redirect('mahasiswa/riwayat');
					}
				}
				$data = array(
					'tgl_sewa'=>$durasi,
					'lama_sewa'=>$this->input->post('xlama'),
					'create_at'=>date('Y-m-d'),
					'mahasiswa_nim'=>$this->input->post('xnim'),
					'tujuan_penyewaan'=>$this->input->post('xtujuan'),
					'kode_penyewaan' =>$kodefix,
					'tipe_peminjaman'=>2,
					'barang_id'=>$this->input->post('xbarangid'),
					'status_transaksi'=>1,
					'sampel' => $this->input->post('xsampel'),
					'total'=>$this->m_admin_barang->ambil_harga($this->input->post('xbarangid'))*$this->input->post('xlama')
				);
				$query = $this->m_transaksi->simpan_transaksi_perhari($data);
				if($query){
					$this->session->set_flashdata('pesan','Gagal Melakukan Transaksi.! Anda Belum Terverifikasi');
				}
				$this->session->set_flashdata('pesan','Transaksi Berhasil');
				redirect('mahasiswa/riwayat');

            }else{
                $this->session->set_flashdata('pesan','Gagal Melakukan Transaksi.! Anda Belum Terverifikasi');
                redirect('mahasiswa/profile');
            }
        }
    }
    public function simpan_transaksi_perjam(){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahasiswa/login');
        }else{
            //$rest = substr(sha1($this->input->post('xtujuan').$durasi.$this->input->post('xbarangid')), 0, 6);  // buat kode pertama
            //$kodefix = $this->input->post('xnim').$rest;
            $nim = $this->session->userdata('mahasiswa_nim');
			$cek_status = $this->m_mhs_login->cek_status_penyewa($nim); // ini untuk pengecekan berkas benar atau tidak
			$mahasiswa_status = $this->m_mhs_login->status_mahasiswa($nim); // ini untuk pengecekan apakah sudah melebihi waktu peminjaman atau belum
			
			if($mahasiswa_status == 1){
                $this->session->set_flashdata('pesan','Gagal Melakukan Transaksi.! Anda Sudah Di Non-Akftifkan');
				redirect('mahasiswa/profile');
			}

            if($cek_status == 2){
                $waktu = array(); // simpan rentang waktu ke dalam array
                $durasinya = $this->input->post('xdurasi');
                $jumlahdurasi = count($durasinya);
                for($x=0;$x<$jumlahdurasi;$x++){
                    array_push($waktu, $durasinya[$x]); // simpan rentang waktu ke dalam array
                }
                $durasi = implode(",",$waktu); // ubah array ke string
                $rest = substr(sha1($this->input->post('xtujuan').$durasi.$this->input->post('xbarangid')), 0, 6);  // buat kode pertama
                $kodefix = $this->input->post('xnim').$rest; // kode peminjaman fix
                $data = array(
                    'tgl_sewa'=>$this->input->post('xtanggal'),
                    'create_at'=>date('Y-m-d'),
                    'durasi_jam'=>$durasi,
                    'mahasiswa_nim'=>$this->input->post('xnim'),
                    'tujuan_penyewaan'=>$this->input->post('xtujuan'),
                    'kode_penyewaan' =>$kodefix,
                    'barang_id'=>$this->input->post('xbarangid'),
                    'tipe_peminjaman'=>1,
                    'status_transaksi'=>1,
                    'sampel'=>$this->input->post('xsampel'),
                    'total'=>$this->m_admin_barang->ambil_harga($this->input->post('xbarangid'))*$jumlahdurasi
                );
                $query = $this->m_transaksi->simpan_transaksi_perjam($data);
                if($query){
                    $this->session->set_flashdata('pesan','Gagal Melakukan Transaksi.! Anda Belum Terverifikasi');
                }
                $this->session->set_flashdata('pesan','Transaksi Berhasil');
				redirect('mahasiswa/riwayat');

            }else{
                $this->session->set_flashdata('pesan','Gagal Melakukan Transaksi.! Anda Belum Terverifikasi');
				redirect('mahasiswa/profile');
            }
        }
    }
    public function batalkan_transaksi($kode_penyewaan){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahasiswa/login');
        }else{
            $data = array(
                'status_transaksi'=>4
            );
            print_r($data);
            $query = $this->m_transaksi->batalkan_transaksi($kode_penyewaan, $data);
            if($query){
                $this->session->set_flashdata('pesan','Penyewaan Berhasil Di Batalkan');
            }
            redirect('mahasiswa/riwayat');
        }
    }
    public function simpan_peminjaman_ruangan(){
        if($this->session->userdata('mahasiswa_nim')==null){
            redirect('mahassiwa/home');
        }else{
            $waktu = array(); // simpan rentang waktu ke dalam array
            $durasinya = $this->input->post('xdurasi');
            $jumlahdurasi = count($durasinya);
            for($x=0;$x<$jumlahdurasi;$x++){
                array_push($waktu, $durasinya[$x]); // simpan rentang waktu ke dalam array
            }
            $durasi = implode(",",$waktu); // ubah array ke string
            $rest = substr(sha1($this->input->post('xtujuan').$durasi.$this->input->post('xidruang')), 0, 6);  // buat kode pertama
            $kodefix = $this->input->post('xnim').$rest; // kode peminjaman fix
            $data = array(
                'tgl_sewa'=>$this->input->post('xtanggal'),
                'create_at'=>date('Y-m-d'),
                'durasi_jam'=>$durasi,
                'mahasiswa_nim'=>$this->input->post('xnim'),
                'alasan_peminjaman'=>$this->input->post('xtujuan'),
                'kode_peminjaman_ruangan' =>$kodefix,
                'ruang_id'=>$this->input->post('xidruang'),
                'status_transaksi'=>1,
            );
            $query = $this->m_transaksi->simpan_transaksi_ruangan($data);
            $this->session->set_flashdata('pesan','Transaksi Berhasil');
            redirect('mahasiswa/riwayat');
        }
    }
}
?>
