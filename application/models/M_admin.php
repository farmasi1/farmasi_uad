<?php
class M_admin extends CI_model{

    public function data_mahasiswa(){
        $this->db->select("*");
        $this->db->from("tbl_mahasiswa");
        $this->db->order_by('mahasiswa_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function data_detail_mahasiswa($id){
        $this->db->where('mahasiswa_id', $id); // Produces: WHERE name = 'Joe'
        $query = $this->db->get('tbl_mahasiswa');
        return $query->result();   
    }



}
    


?>