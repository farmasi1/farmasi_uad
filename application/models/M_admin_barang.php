<?php
class M_admin_barang extends CI_model{

    public function list_barang(){
		$this->db->order_by('barang_id', 'DESC');
        $query = $this->db->get('tbl_barang');
        return $query->result();
    }
    public function simpan_barang($data){
        $this->db->insert('tbl_barang',$data);
    }
    public function simpan_barang_tanpa_image($data){
        $this->db->insert('tbl_barang',$data);
    }
    public function update_barang($data, $id){
        $this->db->where('barang_id', $id);
        $this->db->update('tbl_barang', $data);
    }
    public function update_barang_tanpa_iamge($data, $id){
        $this->db->where('barang_id', $id);
        $this->db->update('tbl_barang', $data);
    }
    public function hapus_barang($id){
        $this->db->where('barang_id', $id);
        $this->db->delete('tbl_barang');
    }
    public function ambil_harga($id){
        $query = $this->db->query("SELECT barang_harga FROM tbl_barang WHERE barang_id='$id'");
        return $query->row()->barang_harga;
    }
    public function total_barang(){
        $query = $this->db->query("SELECT COUNT(*) as total_barang FROM tbl_barang");
        $row = $query->row();
        if(isset($row)){
            return $row->total_barang;
        }
    }
    public function total_ruangan(){
        $query = $this->db->query("SELECT COUNT(*) as total_ruangan FROM tbl_ruangan");
        $row = $query->row();
        if(isset($row)){
            return $row->total_ruangan;
        }
    }
    public function total_ruang_masuk($tahun,$bulan){
        $query = $this->db->query("SELECT COUNT(*) as total_ruangan_masuk FROM tbl_transaksi_ruangan WHERE status_transaksi=2 AND YEAR(create_at)='$tahun' AND MONTH(create_at)='$bulan'");
        $row = $query->row();
        if (isset($row))
        {
            return $row->total_ruangan_masuk;
        }
    }

    public function ruangan(){
        $query = $this->db->get('tbl_ruangan');
        return $query->result();
    }
    public function simpan_ruangan($data){
        $this->db->insert('tbl_ruangan', $data);
    }
    public function hapus_ruangan($data){
        $this->db->delete('tbl_ruangan',$data);
    }
    public function edit_ruangan($data, $id){
        $this->db->where('ruang_id', $id);
        $this->db->update('tbl_ruangan', $data);
    }
    public function jenis_barang($id){
        $query = $this->db->query("SELECT barang_tipe_peminjaman FROM tbl_barang WHERE barang_id='$id'");
        return $query->row()->barang_tipe_peminjaman;
    }
    public function nama_barang($id){
        $query = $this->db->query("SELECT barang_nama FROM tbl_barang WHERE barang_id='$id'");
        return $query->row()->barang_nama;
    }
    public function harga_barang($id){
        $query = $this->db->query("SELECT barang_harga FROM tbl_barang WHERE barang_id='$id'");
        return $query->row()->barang_harga;
    }
    public function list_tanggal_sewa($id, $tahun, $bulan){
        $query = $this->db->query("SELECT tgl_sewa FROM tbl_transaksi WHERE YEAR(create_at)='$tahun' AND MONTH(create_at)='$bulan' AND barang_id='$id'");
        return $query->result();
    }
    public function list_tanggal_sewa_jam($id, $tahun, $bulan){
        $query = $this->db->query("SELECT tgl_sewa FROM tbl_transaksi WHERE YEAR(create_at)='$tahun' AND MONTH(create_at)='$bulan' AND barang_id='$id'");
        return $query->result();
    }
    public function list_tanggal_sewa_tahun($id, $tahun){
        $query = $this->db->query("SELECT tgl_sewa FROM tbl_transaksi WHERE YEAR(create_at)='$tahun' AND barang_id='$id'");
        return $query->result();
    }
    public function total_harga($id, $tahun, $bulan){
        $query = $this->db->query("SELECT SUM(total) as total FROM tbl_transaksi WHERE YEAR(create_at)='$tahun' AND MONTH(create_at)='$bulan' AND barang_id='$id'");
        $row = $query->row();
        if(isset($row)){
            return $row->total;
        }
    }
    public function total_harga_tahun($id, $tahun){
        $query = $this->db->query("SELECT SUM(total) as total FROM tbl_transaksi WHERE YEAR(create_at)='$tahun' AND barang_id='$id'");
        $row = $query->row();
        if(isset($row)){
            return $row->total;
        }
    }
    public function list_durasi_tersewa($id, $tahun, $bulan){
        $query = $this->db->query("SELECT durasi_jam FROM tbl_transaksi WHERE YEAR(create_at)='$tahun' AND MONTH(create_at)='$bulan' AND barang_id='$id'");
        return $query->result();
    }
    public function list_durasi_tersewa_tahun($id, $tahun){
        $query = $this->db->query("SELECT durasi_jam FROM tbl_transaksi WHERE YEAR(create_at)='$tahun' AND barang_id='$id'");
        return $query->result();
    }
    public function ambil_jam($id, $tanggal){
        $query = $this->db->query("SELECT durasi_jam FROM tbl_transaksi WHERE tgl_sewa='$tanggal'");
        return $query->result();
    }
    public function tanggal_tersewa(){
        $query = $this->db->query("SELECT tgl_sewa FROM tbl_transaksi WHERE tipe_peminjaman='2'");
        return $query->result();
	}
	public function detail_barang($id){
        $this->db->where('barang_id', $id);
		$query = $this->db->get('tbl_barang');
		return $query->result();
	}
}
?>
