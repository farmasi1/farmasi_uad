<?php
class M_mahasiswa extends CI_model{

    public function daftar_mahasiswa($data){
        $this->db->insert('tbl_mahasiswa', $data);
    }
    public function cek_jenis($id){
        $query = $this->db->query("SELECT mahasiswa_jenis FROM tbl_mahasiswa WHERE mahasiswa_id ='$id'");
        return $query->row()->mahasiswa_jenis;
    }
    public function update_skripsi($id,$data){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
    }
    public function update_bukti($id,$data){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
    }
    public function update_izin($id,$data){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
    }
    public function update_ktp($id,$data){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
    }
    public function update_verifikasi($id,$data){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
    }
    public function status_verifikasi($nim){
        $query = $this->db->query("SELECT mahasiswa_verifikasi FROM tbl_mahasiswa WHERE mahasiswa_nim ='$nim'");
        return $query->row()->mahasiswa_verifikasi;
    }

    public function status_verifikasi_mahasiswa($id){
        $query = $this->db->query("SELECT mahasiswa_verifikasi FROM tbl_mahasiswa WHERE mahasiswa_id ='$id'");
        return $query->row()->mahasiswa_verifikasi;
	}
	public function status_pengguna($id){
        $query = $this->db->query("SELECT mahasiswa_status FROM tbl_mahasiswa WHERE mahasiswa_id ='$id'");
        return $query->row()->mahasiswa_status;
    }
    public function status_mahasiswa($id){
        $query = $this->db->query("SELECT mahasiswa_jenis FROM tbl_mahasiswa WHERE mahasiswa_id ='$id'");
        return $query->row()->mahasiswa_jenis;
    }
    public function riwayat_peminjaman($nim){
        $this->db->select('*');
        $this->db->from('tbl_transaksi');
        $this->db->join('tbl_barang', 'tbl_transaksi.barang_id = tbl_barang.barang_id');
        $this->db->join('tbl_mahasiswa', 'tbl_transaksi.mahasiswa_nim = tbl_mahasiswa.mahasiswa_nim');
        $this->db->where('tbl_transaksi.mahasiswa_nim', $nim);
        $this->db->order_by('tbl_transaksi.transaksi_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function riwayat_peminjaman_ruangan($nim){
        $this->db->select('*');
        $this->db->from('tbl_transaksi_ruangan');
        $this->db->join('tbl_ruangan', 'tbl_transaksi_ruangan.ruang_id = tbl_ruangan.ruang_id');
        $this->db->where('tbl_transaksi_ruangan.mahasiswa_nim', $nim);
        $this->db->order_by('tbl_transaksi_ruangan.transaksi_ruang_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function ambil_nim($id){
        $query = $this->db->query("SELECT mahasiswa_nim FROM tbl_mahasiswa WHERE mahasiswa_id='$id'");
        return $query->row()->mahasiswa_nim;
    }
    public function total_penyewaan($nim){
        $query = $this->db->query("SELECT count(*) as total_penyewaan FROM tbl_transaksi WHERE mahasiswa_nim='$nim'");
        $row = $query->row();
        if (isset($row))
        {
            return $row->total_penyewaan;
        }
    }
    public function total_biaya($nim){
        $query = $this->db->query("SELECT sum(total) as total_biaya FROM tbl_transaksi WHERE mahasiswa_nim='$nim' AND status_transaksi=2");
        $row = $query->row();
        if (isset($row))
        {
            return $row->total_biaya;
        }
    }
    public function total_pengguna(){
        $query = $this->db->query("SELECT COUNT(*) as total_pengguna FROM tbl_mahasiswa");
        $row = $query->row();
        if(isset($row)){
            return $row->total_pengguna;
        }
    }
    public function tolak_verifikasi($id, $data){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
    }
    public function riwayat_sewa_mahasiswa_jam($nim){
        $this->db->select('*');
        $this->db->from('tbl_transaksi');
        $this->db->join('tbl_barang', 'tbl_transaksi.barang_id = tbl_barang.barang_id');
        $this->db->join('tbl_mahasiswa', 'tbl_transaksi.mahasiswa_nim = tbl_mahasiswa.mahasiswa_nim');
        $this->db->where('tbl_transaksi.mahasiswa_nim', $nim);
        $this->db->where('tbl_transaksi.status_transaksi', 2);
        $this->db->where('tbl_transaksi.tipe_peminjaman', 1);
        $this->db->order_by('tbl_transaksi.transaksi_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function riwayat_sewa_mahasiswa_hari($nim){
        $this->db->select('*');
        $this->db->from('tbl_transaksi');
        $this->db->join('tbl_barang', 'tbl_transaksi.barang_id = tbl_barang.barang_id');
        $this->db->join('tbl_mahasiswa', 'tbl_transaksi.mahasiswa_nim = tbl_mahasiswa.mahasiswa_nim');
        $this->db->where('tbl_transaksi.mahasiswa_nim', $nim);
        $this->db->where('tbl_transaksi.status_transaksi', 2);
        $this->db->where('tbl_transaksi.tipe_peminjaman', 2);
        $this->db->order_by('tbl_transaksi.transaksi_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
	}
	
	public function non_aktifkan($id, $data){
		$this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
	}
	
	public function re_aktifkan($id, $data){
		$this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
	}

}
    


?>
