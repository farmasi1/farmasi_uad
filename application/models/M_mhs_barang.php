<?php 
class M_mhs_barang extends CI_Model{
    public function list_barang(){
		$this->db->limit(8);
		$this->db->order_by('barang_id', 'DESC');
        $query = $this->db->get('tbl_barang');
        return $query->result();
	}
	public function list_barang_limit(){
		$this->db->order_by('barang_id', 'DESC');
        $query = $this->db->get('tbl_barang');
        return $query->result();
	}
    public function detail_barang($id){
        $this->db->where('barang_id', $id);
        $query = $this->db->get('tbl_barang');
        return $query->result();
    }
    public function ambil_tanggal($id){
        //$this->db->query("SELECT tgl_sewa, durasi_jam FROM tbl_transaksi WHERE transaksi_id='$id'");
        $this->db->select('tgl_sewa, durasi_jam');
        $this->db->from('tbl_transaksi');
        $this->db->where('barang_id', $id);
        $this->db->where('barang_id', $id);
        $this->db->where('status_transaksi !=', 3);
        $this->db->where('status_transaksi !=', 4);
        $query = $this->db->get();
        return $query->result();
    }

    public function cek_status_barang($id){
        $query = $this->db->query("SELECT barang_tipe_peminjaman FROM tbl_barang WHERE barang_id='$id'");
        return $query->row()->barang_tipe_peminjaman;
    }

    public function list_jam(){
        $query = $this->db->get('tbl_jam');
        return $query->result();
    }
    public function durasi_tersewa($id, $tanggal){
        $query = $this->db->query("SELECT durasi_jam FROM tbl_transaksi WHERE barang_id='$id' AND tgl_sewa='$tanggal' AND status_transaksi != 4 AND status_transaksi != 3");
        return $query->result();
    }
    public function min_jam($id){
        $query = $this->db->query("SELECT barang_min_jam FROM tbl_barang WHERE barang_id='$id'");
        $row = $query->row();
        if (isset($row))
        {
            return $row->barang_min_jam;
        }
    }
    public function max_jam($id){
        $query = $this->db->query("SELECT barang_max_jam FROM tbl_barang WHERE barang_id='$id'");
        $row = $query->row();
        if (isset($row))
        {
            return $row->barang_max_jam;
        }
    }

    public function max_hari($id){
        $query = $this->db->query("SELECT barang_max_hari FROM tbl_barang WHERE barang_id='$id'");
        $row = $query->row();
        if (isset($row))
        {
            return $row->barang_max_hari;
        }
    }

    public function ambil_jam($kode){
        $query = $this->db->query("SELECT durasi_jam FROM tbl_transaksi WHERE kode_penyewaan='$kode'");
        return $query->result();
    }
    public function ambil_tanggal_riwayat($kode){
        $query = $this->db->query("SELECT tgl_sewa FROM tbl_transaksi WHERE kode_penyewaan='$kode'");
        return $query->result();
    }
    public function cari_barang($search){
        $query = $this->db->query("SELECT *FROM tbl_barang WHERE barang_nama LIKE '%$search%' OR barang_keterangan LIKE '%$search%' OR barang_harga LIKE '%$search%'");
        return $query->result();
    }
}
?>
