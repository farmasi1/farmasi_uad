<?php
class M_mhs_login extends CI_model{

    public function daftar_mahasiswa($data){
        $this->db->insert('tbl_mahasiswa', $data);
    }

    public function login_mhs($email,$pass){

        $this->db->select('*');
        $this->db->from('tbl_mahasiswa');
        $this->db->where('mahasiswa_email',$email);
        $this->db->where('mahasiswa_password',$pass);

        if($query=$this->db->get())
        {
            return $query->row_array();
        }
        else{
            return false;
        }
    }
    public function cek_email($email){

        $this->db->select('*');
        $this->db->from('tbl_mahasiswa');
        $this->db->where('mahasiswa_email',$email);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }

    public function email_checkpass($email){
        $query = $this->db->query("SELECT mahasiswa_email FROM tbl_mahasiswa WHERE mahasiswa_email='$email'");
        return $query->result();
    }
    

    public function update_id($email_sekolah,$id_pelanggan){
        $query = $this->db->query("UPDATE tbl_customer set id_customer='$id_pelanggan' WHERE email_sekolah='$email_sekolah'");
        return $query;
    }

    public function riwayat_customer(){
        $idcustomer = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_transaksi.banyak_barang) as banyak_barang,tbl_transaksi.kode_transaksi, tbl_transaksi.tanggal, sum(tbl_transaksi.total_harga) as total_harga, tbl_transaksi.status_transaksi AS status 
        FROM tbl_transaksi WHERE tbl_transaksi.id_sekolah='$idcustomer' GROUP BY tbl_transaksi.kode_transaksi ORDER BY tbl_transaksi.tanggal DESC");
        return $query->result();
    }

    public function riwayat_proses(){
        $idcustomer = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_pesanan.jumlah_buku) as banyak_barang, tbl_pesanan.tanggal, sum(tbl_pesanan.total_harga) as total_harga, tbl_pesanan.id_transaksi as kode_transaksi 
        FROM tbl_pesanan WHERE tbl_pesanan.id_pembeli='$idcustomer' GROUP BY tbl_pesanan.tanggal ORDER BY tbl_pesanan.tanggal DESC");
        return $query->result();
    }

    public function ambil_id($email){
        $query = $this->db->query("SELECT mahasiswa_id FROM tbl_mahasiswa WHERE mahasiswa_email='$email'");
        return $query->row()->mahasiswa_id;
    }

    function ambilmahasiswa($email){
        $query = $this->db->get_where('tbl_mahasiswa',array('email_mahasiswa'=>$email));
		return $query->row_array();
    }

    function activin($email){
        $status = 1;
        $query = $this->db->query("UPDATE tbl_mahasiswa set mahasiswa_aktif='$status' WHERE mahasiswa_email='$email'");
        return $query;
    }

    function ambil_status($email){
        $query = $this->db->query("SELECT mahasiswa_aktif FROM tbl_mahasiswa WHERE mahasiswa_email='$email'");
        return $query->row()->mahasiswa_aktif;
    }

    function ambil_email($id){
        $query = $this->db->query("SELECT mahasiswa_email FROM tbl_mahasiswa WHERE mahasiswa_id='$id'");
        return $query->row()->mahasiswa_email;
    }
    
    function passworbaru($id, $password){
        $query = $this->db->query("UPDATE tbl_mahasiswa set password='$password' WHERE id='$id'");
        return $query;
    }
    function list_mahasiswa(){
        $query = $this->db->query("SELECT *FROM tbl_mahasiswa");
        return $query->result();
    }
    function banyak_mahasiswa(){
        $query = $this->db->query("SELECT count(id) as banyakmahasiswa FROM tbl_mahasiswa");
        return $query->row()->banyakmahasiswa;
    }
    function indentitas_mahasiswa($nim){
        $this->db->select('*');
        $this->db->from('tbl_mahasiswa');
        $this->db->where('mahasiswa_nim',$nim);
        $query = $this->db->get();
        return $query->result();
    }
    function ambil_password($id){
        $query = $this->db->query("SELECT mahasiswa_password FROM tbl_mahasiswa WHERE mahasiswa_id='$id'");
        return $query->row()->mahasiswa_password;
    }
    function update_profile($data, $id){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
    }
    public function cek_status_penyewa($nim){
        $query = $this->db->query("SELECT mahasiswa_verifikasi FROM tbl_mahasiswa WHERE mahasiswa_nim='$nim'");
        return $query->row()->mahasiswa_verifikasi;
	}
	
	public function status_mahasiswa($nim){
        $query = $this->db->query("SELECT mahasiswa_status FROM tbl_mahasiswa WHERE mahasiswa_nim='$nim'");
        return $query->row()->mahasiswa_status;
	}
	
    public function biodata($nim){
        $query = $this->db->query("SELECT *FROM tbl_mahasiswa WHERE mahasiswa_nim='$nim'");
        return $query->result();
    }
    public function simpan_password_baru($id, $data){
        $this->db->where('mahasiswa_id', $id);
        $this->db->update('tbl_mahasiswa', $data);
        return TRUE;
    }



}
    


?>
