<?php
class M_transaksi extends CI_model{
    public function simpan_transaksi_perhari($data){
        $this->db->insert('tbl_transaksi', $data);
    }
    public function tranaksi_masuk(){
        $this->db->select('*');
        $this->db->from('tbl_transaksi');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.mahasiswa_nim = tbl_transaksi.mahasiswa_nim');
        $this->db->join('tbl_barang', 'tbl_barang.barang_id = tbl_transaksi.barang_id');
        $this->db->where('tbl_transaksi.status_transaksi', 1);
        $this->db->order_by('tbl_transaksi.transaksi_id', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }
    public function simpan_transaksi_perjam($data){
        $this->db->insert('tbl_transaksi', $data);
    }
    public function simpan_transaksi_ruangan($data){
        $this->db->insert('tbl_transaksi_ruangan', $data);
    }
    public function tolak_penyewaan($kode_penyewaan, $data){
        $this->db->where('kode_penyewaan', $kode_penyewaan);
        $this->db->update('tbl_transaksi', $data);
    }
    public function transaksi_diterima(){
        $this->db->select('*');
        $this->db->from('tbl_transaksi');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.mahasiswa_nim = tbl_transaksi.mahasiswa_nim');
        $this->db->join('tbl_barang', 'tbl_barang.barang_id = tbl_transaksi.barang_id');
        $this->db->where('tbl_transaksi.status_transaksi', 2);
        $this->db->order_by('tbl_transaksi.transaksi_id', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }
    public function terima_penyewaan($kode_penyewaan, $data){
        $this->db->where('kode_penyewaan', $kode_penyewaan);
        $this->db->update('tbl_transaksi', $data);
    }
    public function transaksi_ditolak(){
        $this->db->select('*');
        $this->db->from('tbl_transaksi');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.mahasiswa_nim = tbl_transaksi.mahasiswa_nim');
        $this->db->join('tbl_barang', 'tbl_barang.barang_id = tbl_transaksi.barang_id');
        $this->db->where('tbl_transaksi.status_transaksi', 3);
        $this->db->order_by('tbl_transaksi.transaksi_id', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }
    public function ambil_jam($key){
        $query = $this->db->query("SELECT jam_waktu FROM tbl_jam WHERE jam_sesi='$key'");
        return $query->row()->jam_waktu;
    }
    public function batalkan_transaksi($kode_penyewaan, $data){
        $this->db->where('kode_penyewaan', $kode_penyewaan);
        $this->db->update('tbl_transaksi', $data);
        return TRUE;
    }
    public function transaksi_masuk(){
        $query = $this->db->query("SELECT COUNT(*) as total FROM tbl_transaksi WHERE status_transaksi=1");
        $row = $query->row();
        if (isset($row))
        {
            return $row->total;
        }
    }
    public function total_transaksi($tahun,$bulan){
        $query = $this->db->query("SELECT COUNT(*) as total_transaksi FROM tbl_transaksi WHERE status_transaksi=2 AND YEAR(create_at)='$tahun' AND MONTH(create_at)='$bulan'");
        $row = $query->row();
        if (isset($row))
        {
            return $row->total_transaksi;
        }
    }
    public function total_pendapatan($tahun,$bulan){
        $query = $this->db->query("SELECT SUM(total) as total_pendapatan FROM tbl_transaksi WHERE status_transaksi=2 AND YEAR(create_at)='$tahun' AND MONTH(create_at)='$bulan'");
        $row = $query->row();
        if (isset($row))
        {
            return $row->total_pendapatan;
        }
    }

    public function tranaksi_masuk_ruangan(){
        $this->db->select('*');
        $this->db->from('tbl_transaksi_ruangan');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.mahasiswa_nim = tbl_transaksi_ruangan.mahasiswa_nim');
        $this->db->join('tbl_ruangan', 'tbl_ruangan.ruang_id = tbl_transaksi_ruangan.ruang_id');
        $this->db->where('tbl_transaksi_ruangan.status_transaksi', 1);
        $this->db->order_by('tbl_transaksi_ruangan.transaksi_ruang_id', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }
    public function terima_penyewaan_ruangan($kode, $data){
        $this->db->where('kode_peminjaman_ruangan', $kode);
        $this->db->update('tbl_transaksi_ruangan', $data);
    }
    public function tolak_penyewaan_ruangan($kode, $data){
        $this->db->where('kode_peminjaman_ruangan', $kode);
        $this->db->update('tbl_transaksi_ruangan', $data);
    }

    public function transaksi_diterima_ruangan(){
        $this->db->select('*');
        $this->db->from('tbl_transaksi_ruangan');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.mahasiswa_nim = tbl_transaksi_ruangan.mahasiswa_nim');
        $this->db->join('tbl_ruangan', 'tbl_ruangan.ruang_id = tbl_transaksi_ruangan.ruang_id');
        $this->db->where('tbl_transaksi_ruangan.status_transaksi', 2);
        $this->db->order_by('tbl_transaksi_ruangan.transaksi_ruang_id', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }
    public function transaksi_ditolak_ruangan(){
        $this->db->select('*');
        $this->db->from('tbl_transaksi_ruangan');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.mahasiswa_nim = tbl_transaksi_ruangan.mahasiswa_nim');
        $this->db->join('tbl_ruangan', 'tbl_ruangan.ruang_id = tbl_transaksi_ruangan.ruang_id');
        $this->db->where('tbl_transaksi_ruangan.status_transaksi', 3);
        $this->db->order_by('tbl_transaksi_ruangan.transaksi_ruang_id', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }

}
?>