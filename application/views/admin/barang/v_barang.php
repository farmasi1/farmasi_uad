<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin FARMASI UAD</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

 <?php $this->load->view('admin/partials/upper-section.php'); ?>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
    
      $this->load->view("admin/partials/header.php");
      $this->load->view("admin/partials/nav-sidebar.php");
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Peminjaman Ruangan</li>
      </ol>
    </section>  
        <section class="content">

               <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Barang</h3>
            </div>
                            <!-- alert -->
                            <?php $pesan = $this->session->flashdata('pesan'); 
                if($pesan){
                ?>
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $pesan?></strong>
                </div>
                <?php }?>
                <!-- alert -->

            
            
            <div class="box-body">
              <div class="col">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pinjamModal">
                    <i class="fa fa-plus"></i> Tambah Data
                  </button>
                  <!-- <button type="button" class="btn btn-info" data-toggle="modal" data-target="#export">
                    Export
                  </button> -->
                  <!-- <a href="<?php echo base_url().'admin/barang/export_excel'?>" class="btn btn-primary">Export Excel</a> -->
                </div>
                <br>
              <table id="example1" class="table table-bordered table-hover">
                
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Harga Sewa</th>
                    <th>Kondisi</th>
                    <th>Keterangan Kondisi</th>
                    <th>Gambar</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0; foreach($barang as $data): $no++?>
                    <tr>
                        <td><?php echo $no?></td>
                        <td><?php echo $data->barang_nama?></td>
                        <?php if($data->barang_tipe_peminjaman == 1){?>
                          <td><?php echo $data->barang_harga." / Hari"?></td>
                        <?php }else{?>
                          <td><?php echo $data->barang_harga." / Jam"?></td>
                        <?php }?>
                        <td><?php echo $data->barang_kondisi?></td>
                        <td><?php echo $data->barang_keterangan?></td>
                        <td><img src="<?php echo base_url('assets/images/barang/'.$data->barang_foto) ?>" width="64" /></td>
                        <td style="width: 75px;"><div class="row">
                          <a data-toggle="modal" data-target="#ModalEdit<?php echo $data->barang_id?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a>
                          <a data-toggle="modal" data-target="#ModalHapus<?php echo $data->barang_id?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 20px; font-weight: bold;"></i> </a>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.
      /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>

  <!-- ============================= MODAL PEMINJAMAN ========================= -->
  <div class="modal fade" id="pinjamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
      
        <form action="<?php echo site_url('admin/barang/simpan_barang')?>" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="usr">Nama Barang:</label>
                <input type="text" class="form-control" name="xnama" required>
            </div>
            <div class="form-group">
                <label for="usr">Harga Sewa:</label>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="xjenis" value="1" checked>Per Hari
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="xjenis" value="2">Per Jam
                  </label>
                </div>
                <input type="number" class="form-control" id="usr" name="xharga" required>
            </div>
            <div class="form-group">
              <label for="sel1">Kondisi Barang:</label>
              <select class="form-control" id="sel1" name="xkondisi">
                <option value="Terawat">Terawat</option>
                <option value="TIdak Terawat">TIdak Terawat</option>
              </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan:</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" required>
            </div>
            <div class="form-group">
                <label for="usr">Gambar Barang:</label>
                <input type="file" class="form-control-file border" name="filefoto" required>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
        
    </div>
  </div>
</div>
 <!-- =================================================================================== -->

  <!-- ============================= Edit ========================= -->
  <?php foreach($barang as $data):?>
  <form action="<?php echo site_url('admin/barang/edit_barang')?>" method="POST"  enctype="multipart/form-data">
  <div class="modal fade" id="ModalEdit<?php echo $data->barang_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cek Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group" hidden>
                <label for="usr">Id barang:</label>
                <input type="text" class="form-control" name="xid" value="<?php echo $data->barang_id?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Nama Barang:</label>
                <input type="text" class="form-control" name="xnama" value="<?php echo $data->barang_nama?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Harga Sewa:</label>
                <?php if($data->barang_tipe_peminjaman == 1){?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="xjenis" value="1" checked>Per Hari
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="xjenis" value="2">Per Jam
                  </label>
                </div>
                <?php }else{?>
                  <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="xjenis" value="1">Per Hari
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="xjenis" value="2" checked>Per Jam
                  </label>
                </div>
                <?php }?>
                <input type="number" class="form-control" id="usr" name="xharga" value="<?php echo $data->barang_harga?>" required>
            </div>
            <div class="form-group">
              <label for="sel1">Kondisi Barang:</label>
              <select class="form-control" id="sel1" name="xkondisi">
                <option value="Terawat">Terawat</option>
                <option value="TIdak Terawat">TIdak Terawat</option>
              </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan Kondisi:</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" value="<?php echo $data->barang_keterangan ?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Gambar Barang:</label>
                <input type="file" class="form-control-file border" name="filefoto">
            </div>
      <div class="container">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </div>
  </div>
</div>
</form>
<?php endforeach?>
 <!-- =================================================================================== -->


  <!-- ============================= HAPUS ========================= -->
  <?php foreach($barang as $data):?>
  <div class="modal fade" id="ModalHapus<?php echo $data->barang_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('admin/barang/hapus_barang/'.$data->barang_id)?>" metdod="POST">
      <div class="modal-body">
        <div class="container">
          <h5>Apakah anda yakin menghapus barang <strong><?php echo $data->barang_nama?></strong> .?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->

  <!-- ============================= Export ========================= -->
    <div class="modal" id="export">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Export Laporan Barang</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
        <form action="<?php echo site_url('admin/barang/export')?>" method="POST">
          <div class="modal-body">
            <div class="form-group">
                <label for="sel1">Laboratorium</label>
                <select class="form-control" id="sel1" name="xlab">
                <?php foreach($ruang as $key):?>
                  <option value=<?php echo $key->id_ruang?>><?php echo $key->nama_ruang?></option>
                <?php endforeach?>
                </select>
              </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Export</button>
          </div>
        </form>

        </div>
      </div>
    </div>
 <!-- =================================================================================== -->



  <!-- /.content-wrapper -->
 <?php 
    $this->load->view('admin/partials/footer.php');
  ?>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/adminlte/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url('assets/adminlte/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/adminlte/dist/js/demo.js')?>"></script>

<script>
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
