<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin FARMASI UAD</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

 <?php $this->load->view('admin/partials/upper-section.php'); ?>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
    
      $this->load->view("admin/partials/header.php");
      $this->load->view("admin/partials/nav-sidebar.php");
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Peminjaman Ruangan</li>
      </ol>
    </section>  
        <section class="content">

               <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Barang</h3>
            </div>
                            <!-- alert -->
                            <?php $pesan = $this->session->flashdata('pesan'); 
                if($pesan){
                ?>
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $pesan?></strong>
                </div>
                <?php }?>
                <!-- alert -->

            
            
            <div class="box-body">
              <div class="col">
                  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pinjamModal">
                    <i class="fa fa-plus"></i> Tambah Data
                  </button> -->

                </div>
                <br>
              <table id="example1" class="table table-bordered table-hover">
                
                 <thead>

                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIM/NIP</th>
                    <th>Alamat</th>
                    <th>NO KTP</th>
                    <th>NO Handphone</th>
                    <th>Jenis Kelamin</th>
                    <th>Status</th>
                    <th>Email</th>
                    <th>Gambar</th>
                    <th>Option</th>
                </tr>
                </thead>
                <?php $no=0; foreach($mahasiswa as $data): $no++?>
                    <tr>
                        <td><?php echo $no?></td>
                        <td><?php echo $data->mahasiswa_nama?></td>
                        <td><?php echo $data->mahasiswa_nim?></td>
                        <td><?php echo $data->mahasiswa_alamat?></td>
                        <td><?php echo $data->mahasiswa_ktp?></td>
                        <td><?php echo $data->mahasiswa_nohp?></td>
                        <td><?php echo $data->mahasiswa_jk?></td>
                        <td><?php echo $data->mahasiswa_jenis?></td>
                        <td><?php echo $data->mahasiswa_email?></td>
                        <td><img src="<?php echo base_url('assets/images/barang/'.$data->mahasiswa_foto) ?>" width="64" /></td>
                        <td>
                            <a href="<?php echo site_url('admin/mahasiswa/detail_mahasiswa/'.$data->mahasiswa_id) ?>" class="btn btn-info" role="button">Detail</a>
                        </td>
                    </tr>
                <?php endforeach?>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>

  <!-- ============================= MODAL PEMINJAMAN ========================= -->
  <div class="modal fade" id="pinjamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
      
        <form action="<?php echo site_url('admin/barang/simpan_barang')?>" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="usr">Nama Barang:</label>
                <input type="text" class="form-control" name="xnama" required>
            </div>
            <div class="form-group">
                <label for="usr">Harga Sewa / 24 Jam:</label>
                <input type="number" class="form-control" id="usr" name="xharga" required>
            </div>
            <div class="form-group">
              <label for="sel1">Kondisi Barang:</label>
              <select class="form-control" id="sel1" name="xkondisi">
                <option value="Terawat">Terawat</option>
                <option value="TIdak Terawat">TIdak Terawat</option>
              </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan Kondisi:</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" required>
            </div>
            <div class="form-group">
                <label for="usr">Gambar Barang:</label>
                <input type="file" class="form-control-file border" name="filefoto" required>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
        
    </div>
  </div>
</div>
 <!-- =================================================================================== -->

  <!-- ============================= Edit ========================= -->
  <?php foreach($mahasiswa as $data):?>
  <form action="<?php echo site_url('admin/barang/edit_barang')?>" method="POST"  enctype="multipart/form-data">
  <div class="modal fade" id="ModalEdit<?php echo $data->mahasiswa_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cek Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group" hidden>
                <label for="usr">Id barang:</label>
                <input type="text" class="form-control" name="xid" value="<?php echo $data->mahasiswa_id?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Nama Barang:</label>
                <input type="text" class="form-control" name="xnama" value="<?php echo $data->mahasiswa_nama?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Harga Sewa / 24 Jam:</label>
                <input type="number" class="form-control" id="usr" name="xharga" value="<?php echo $data->barang_harga ?>" required>
            </div>
            <div class="form-group">
              <label for="sel1">Kondisi Barang:</label>
              <select class="form-control" id="sel1" name="xkondisi">
                <option value="Terawat">Terawat</option>
                <option value="TIdak Terawat">TIdak Terawat</option>
              </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan Kondisi:</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" value="<?php echo $data->barang_keterangan ?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Gambar Barang:</label>
                <input type="file" class="form-control-file border" name="filefoto">
            </div>
      <div class="container">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </div>
  </div>
</div>
</form>
<?php endforeach?>
 <!-- =================================================================================== -->


  <!-- ============================= HAPUS ========================= -->
  <?php foreach($mahasiswa as $data):?>
  <div class="modal fade" id="ModalHapus<?php echo $data->mahasiswa_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('admin/barang/hapus_barang/'.$data->mahasiswa_id)?>" metdod="POST">
      <div class="modal-body">
        <div class="container">
          <h5>Apakah anda yakin menghapus <strong><?php echo $data->mahasiswa_nama?></strong> .?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->


  <!-- /.content-wrapper -->
 <?php 
    $this->load->view('admin/partials/footer.php');
  ?>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/adminlte/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url('assets/adminlte/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/adminlte/dist/js/demo.js')?>"></script>

<script>
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
