  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo site_url('assets/adminlte/dist/img/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('nama_admin'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo site_url('admin/admin');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Peminjaman Ruangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li>
              <a href="<?php echo site_url('admin/peminjaman');?>">
              <i class="fa fa-circle-o"></i> Peminjaman Masuk
              </a>
            </li>

            <li>
              <a href="<?php echo site_url('admin/peminjaman/diterima');?>">
              <i class="fa fa-circle-o"></i>Peminjaman Diterima
              </a>
            </li>
            <li>
              <a href="<?php echo site_url('admin/peminjaman/ditolak');?>">
              <i class="fa fa-circle-o"></i>Peminjaman Ditolak
              </a>
            </li>
          </ul>
        </li> -->
        <!-- <li>
          <a href="<?php echo site_url('admin/peminjaman/peminjaman_alat');?>">
            <i class="fa fa-cube"></i> <span>Peminjaman Alat</span>
          </a>
        </li> -->
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Peminjaman Komputer</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li>
              <a href="<?php echo site_url('admin/peminjaman/peminjaman_komputer');?>">
              <i class="fa fa-circle-o"></i> Peminjaman Masuk
              </a>
            </li>

            <li>
              <a href="<?php echo site_url('admin/peminjaman/diterima_komputer');?>">
              <i class="fa fa-circle-o"></i>Peminjaman Diterima
              </a>
            </li>
            <li>
              <a href="<?php echo site_url('admin/peminjaman/ditolak_komputer');?>">
              <i class="fa fa-circle-o"></i>Peminjaman Ditolak
              </a>
            </li>
          </ul>
        </li> -->

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-cube"></i> <span>Peminjaman Alat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li>
              <a href="<?php echo site_url('admin/peminjaman/peminjaman_alat');?>">
              <i class="fa fa-circle-o"></i> Peminjaman Masuk
              </a>
            </li>

            <li>
              <a href="<?php echo site_url('admin/peminjaman/diterima_alat');?>">
              <i class="fa fa-circle-o"></i>Peminjaman Diterima
              </a>
            </li>
            <li>
              <a href="<?php echo site_url('admin/peminjaman/ditolak_alat');?>">
              <i class="fa fa-circle-o"></i>Peminjaman Ditolak
              </a>
            </li>
          </ul>
        </li> -->

        <li class="treeview">
          <a href="#">
            <i class="fa fa-cubes"></i> <span>Barang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li>
              <a href="<?php echo site_url('admin/barang');?>">
              <i class="fa fa-circle-o"></i> Barang Per Hari
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="<?php echo site_url('admin/mahasiswa');?>">
            <i class="fa fa-graduation-cap"></i> <span>Mahasiswa</span>
          </a>
        </li>        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>