<!DOCTYPE html>
<html>
<?php 
    $this->load->view('admin1/partials/head.php');
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
	<?php $this->load->view('admin1/partials/navbar.php') ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo site_url('assets1/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin SISELA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo site_url('assets1/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('admin_nama') ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <?php $this->load->view('admin1/partials/sidebar.php') ?>
      <!-- Sidebar Menu -->
      
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Barang</h3>
            </div>
                <div class="row" style="padding: 10px;">
                    <div class="col-4">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pinjamModal">
                              <i class="fa fa-plus"></i> Tambah Data
                      </button>
                    </div>
                </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Harga Sewa</th>
                    <th>Minimal jam</th>
                    <th>Maksimal jam</th>
                    <th>Maksimal hari</th>
                    <th>Kondisi</th>
                    <th>Keterangan Kondisi</th>
                    <th>Gambar</th>
                    <th>Report</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0; foreach($barang as $data): $no++?>
                    <tr>
                        <td><?php echo $no?></td>
                        <td><?php echo $data->barang_nama?></td>
                        <?php if($data->barang_tipe_peminjaman == 2){?>
                          <td><?php echo $data->barang_harga." / Hari"?></td>
                        <?php }else{?>
                          <td><?php echo $data->barang_harga." / Jam"?></td>
                        <?php }?>
                        <td><?php echo $data->barang_min_jam?></td>
                        <td><?php echo $data->barang_max_jam?></td>
                        <td><?php echo $data->barang_max_hari?></td>
                        <td><?php echo $data->barang_kondisi?></td>
                        <td><?php echo $data->barang_keterangan?></td>
                        <td><img src="<?php echo base_url('assets/images/barang/'.$data->barang_foto) ?>" width="64" /></td>
												<td>
													<a class="btn btn-info" style="color:white;" data-toggle="modal" data-target="#laporanbulan<?php echo $data->barang_id?>" class="btn btn-small">Bulan </a>&nbsp; &nbsp;
                          <a class="btn btn-info" style="color:white;" data-toggle="modal" data-target="#laporantahun<?php echo $data->barang_id?>" class="btn btn-small">Tahun </a>	
												</td>
												<td>
                          <!-- <a data-toggle="modal" data-target="#ModalEdit<?php echo $data->barang_id?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a> -->
                          <a href="<?php echo site_url("admin1/barang/update_barang/$data->barang_id")?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a>
                          <a data-toggle="modal" data-target="#ModalHapus<?php echo $data->barang_id?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 20px; font-weight: bold;"></i> </a>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('admin1/partials/footer.php')?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<?php 
    $this->load->view('admin1/partials/js.php');
?>
</body>
</html>
<!-- ============================= MODAL PEMINJAMAN ========================= -->
<div class="modal fade" id="pinjamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
      
        <form action="<?php echo site_url('admin1/barang/simpan_barang')?>" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="usr">Nama Barang:</label>
                <input type="text" class="form-control" name="xnama" required>
            </div>
            <div class="form-group">
                <label for="usr">Harga Sewa:</label>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" onclick="javascript:harijam();" id="perhari" class="form-check-input" name="xjenis" value="2" >Per Hari
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" onclick="javascript:harijam();" id="perjam" class="form-check-input" name="xjenis" value="1" >Per Jam
                  </label>
                </div>
                <input type="number" class="form-control" id="usr" name="xharga" placeholder="harga" required>
            </div>
            <!-- ini untuk jam -->
            <div id="dataperjam" style="display:none">
              <div class="form-group">
                <label for="usr">Minimal Jam sewa:</label>
                <input type="number" class="form-control" id="usr" name="xminjam">
              </div>
              <div class="form-group">
                <label for="usr">Maximal Jam sewa:</label>
                <input type="number" class="form-control" id="usr" name="xmaxjam">
              </div>
            </div>
            <!-- ini untuk jam -->
            <!-- ini untuk hari -->
            <div id="dataperhari" style="display:none">
              <div class="form-group">
                <label for="usr">Maximal Hari sewa:</label>
                <input type="number" class="form-control" id="usr" name="xmaxhari">
              </div>
            </div>
            <!-- ini untuk hari -->

            <div class="form-group">
              <label for="sel1">Kondisi Barang:</label>
              <select class="form-control" id="sel1" name="xkondisi">
                <option value="Terawat">Terawat</option>
                <option value="TIdak Terawat">TIdak Terawat</option>
              </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan:</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" required>
            </div>
            <div class="form-group">
                <label for="usr">Gambar Barang:</label>
                <input type="file" class="form-control-file border" name="filefoto" required>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
        
    </div>
  </div>
</div>
 <!-- =================================================================================== -->

   <!-- ============================= Edit ========================= -->
  <?php foreach($barang as $data):?>
  <form action="<?php echo site_url('admin1/barang/edit_barang')?>" method="POST"  enctype="multipart/form-data">
    <div class="modal fade" id="ModalEdit<?php echo $data->barang_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cek Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <div class="form-group" hidden>
                    <label for="usr">Id barang:</label>
                    <input type="text" class="form-control" name="xid" value="<?php echo $data->barang_id?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Nama Barang:</label>
                    <input type="text" class="form-control" name="xnama" value="<?php echo $data->barang_nama?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Jenis Barang:</label>
                    <?php if($data->barang_tipe_peminjaman == 1){?>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perjam_edit" name="xjenis" value="1" checked>Per Jam
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perhari_edit" name="xjenis" value="2" >Per Hari
                      </label>
                    </div>
                    <?php }else{?>
                      <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perjam_edit" name="xjenis" value="1">Per Jam
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perhari_edit" name="xjenis" value="2" checked>Per Hari
                      </label>
                    </div>
                    <?php }?>
                    <label for="usr">Harga Sewa:</label>
                    <input type="number" class="form-control" id="usr" name="xharga" value="<?php echo $data->barang_harga?>" required>
								</div>
									<!-- ini untuk jam -->
										<div id="dataperjam_edit" style="display:none">
										<div class="form-group">
											<label for="usr">Minimal Jam sewa:</label>
											<input type="number" class="form-control" id="usr" value="<?php echo $data->barang_min_jam?>" name="xminjam">
										</div>
										<div class="form-group">
											<label for="usr">Maximal Jam sewa:</label>
											<input type="number" class="form-control" id="usr" value="<?php echo $data->barang_max_jam?>" name="xmaxjam">
										</div>
									</div>
									<!-- ini untuk jam -->
									<!-- ini untuk hari -->
									<div id="dataperhari_edit" style="display:none">
										<div class="form-group">
											<label for="usr">Maximal Hari sewa:</label>
											<input type="number" class="form-control" id="usr" value="<?php echo $data->barang_max_hari?>" name="xmaxhari">
										</div>
									</div>
									<!-- ini untuk hari -->
								
                <div class="form-group">
                  <label for="sel1">Kondisi Barang:</label>
                  <select class="form-control" id="sel1" name="xkondisi">
                    <option value="Terawat">Terawat</option>
                    <option value="TIdak Terawat">TIdak Terawat</option>
                  </select>
                </div>
                <div class="form-group">
                    <label for="usr">Keterangan Kondisi:</label>
                    <input type="text" class="form-control" id="usr" name="xketerangan" value="<?php echo $data->barang_keterangan ?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Gambar Barang:</label>
                    <input type="file" class="form-control-file border" name="filefoto">
                </div>
          <div class="container">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-primary">Edit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <?php endforeach?>
    <!-- =================================================================================== -->

    <!-- ============================= Edit ========================= -->
  <?php foreach($barang as $data):?>
  <form action="<?php echo site_url('admin1/barang/edit_barang')?>" method="POST"  enctype="multipart/form-data">
    <div class="modal fade" id="ModalHapus<?php echo $data->barang_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <div class="form-group">
                    <label for="usr">Anda Yakin Ingin Menghapus Barang <?php echo $data->barang_nama?></label>
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            <a href="<?php echo site_url("admin1/barang/hapus_barang/$data->barang_id")?>" type="submit" class="btn btn-danger">Hapus</a>
          </div>
        </div>
      </div>
    </div>
  </form>
  <?php endforeach?>

      <!-- ============================= ini untuk report ========================= -->
      <?php foreach($barang as $data):?>
  <form action="<?php echo site_url('admin1/barang/laporan_perbulan')?>" method="POST"  enctype="multipart/form-data">
    <div class="modal fade" id="laporanbulan<?php echo $data->barang_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cetak Laporan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="text" name="xidbarang" value="<?php echo $data->barang_id ?>" hidden>
          <div class="modal-body">
                  <div class="form-group">
                    <label for="sel1">Pilih Tahun:</label>
                    <select class="form-control" id="sel1" name="xtahun">
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                      <option value="2029">2029</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="sel1">Pilih Bulan:</label>
                    <select class="form-control" id="sel1" name="xbulan">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                  </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-danger">Cetak </button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <?php endforeach?>

  <!-- ============================= ini untuk report ========================= -->
  <?php foreach($barang as $data):?>
  <form action="<?php echo site_url('admin1/barang/laporan_pertahun')?>" method="POST"  enctype="multipart/form-data">
    <div class="modal fade" id="laporantahun<?php echo $data->barang_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cetak Laporan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="text" name="xidbarang" value="<?php echo $data->barang_id ?>" hidden>
          <div class="modal-body">
                  <div class="form-group">
                    <label for="sel1">Pilih Tahun:</label>
                    <select class="form-control" id="sel1" name="xtahun">
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                      <option value="2029">2029</option>
                    </select>
                  </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-danger">Cetak </button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <?php endforeach?>
<script type="text/javascript">
  function harijam() {
      if (document.getElementById('perhari').checked) {
          document.getElementById('dataperhari').style.display = 'block';
      } else document.getElementById('dataperhari').style.display = 'none';
      if (document.getElementById('perjam').checked) {
          document.getElementById('dataperjam').style.display = 'block';
      } else document.getElementById('dataperjam').style.display = 'none';
  }
</script>

<script type="text/javascript">
  function harijam_edit() {
      if (document.getElementById('perhari_edit').checked) {
          document.getElementById('dataperhari_edit').style.display = 'block';
      } else document.getElementById('dataperhari_edit').style.display = 'none';
      if (document.getElementById('perjam_edit').checked) {
          document.getElementById('dataperjam_edit').style.display = 'block';
      } else document.getElementById('dataperjam_edit').style.display = 'none';
  }
</script>

