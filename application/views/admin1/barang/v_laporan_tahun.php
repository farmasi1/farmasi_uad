<?php    $ci =& get_instance();
    $ci->load->model('m_admin_barang');
    $ci->load->model('m_transaksi');
     if($jenis_barnag == 2){ ?>
<!DOCTYPE html>
<html lang="en">
<?php 
    $this->load->view('admin1/partials/head.php');
    ?>
<body>

<div class="container">
    <div class="row">
        <div class="col-12" style="text-align: center;">
            <h3>Laporan Penyewaan Alat Praktikum <br> Fakultas Farmasi <br> Universtias Ahmad Dahlan <?php echo " Tahun ".$tahun ?></h3>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-4">
            Nama Alat
        </div>
        <div class="col-8">
            <?php echo $nama_barang?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Harga sewa/hari
        </div>
        <div class="col-8">
            <?php echo $harga_barang?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            List Tanggal Sewa
        </div>
        <div class="col-8">
            <?php      
            $a = array();
            foreach($tanggal_sewa as $v){
                $loop = explode(",",$v->tgl_sewa);
                foreach ($loop as $key => $value) {
                    $a[] = $value;
                }
            }
            $total = count($a);
            for($i =0; $i<$total; $i++){
                echo $a[$i]."<br>";
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Total Hari Tersewa
        </div>
        <div class="col-8">
            <?php echo $total?> hari
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Total Harga
        </div>
        <div class="col-8">
            <?php echo $total_harga?>
        </div>
    </div>
</div>

</body>
</html>
<?php }elseif($jenis_barnag == 1){?>
    <!DOCTYPE html>
<html lang="en">
<?php 
    $this->load->view('admin1/partials/head.php');
    ?>
<body>

<div class="container">
    <div class="row">
        <div class="col-12" style="text-align: center;">
            <h3>Laporan Penyewaan Alat Praktikum <br> Fakultas Farmasi <br> Universtias Ahmad Dahlan <?php echo " Tahun ".$tahun ?></h3>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-4">
            Nama Alat
        </div>
        <div class="col-8">
            <?php echo $nama_barang?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Harga sewa/Jam
        </div>
        <div class="col-8">
            <?php echo $harga_barang?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            List Tanggal Sewa
        </div>
        <div class="col-8">
            <?php  $total_jam = 0; foreach($tanggal_sewa as $data){
                echo $data->tgl_sewa."<br>";
                $durasi_jam = $ci->m_admin_barang->ambil_jam($id, $data->tgl_sewa);
                foreach($durasi_jam as $datajam){
                    $durasi = explode(",",$datajam->durasi_jam);
                     foreach($durasi as $key){
                         echo "Jam ke ".$key." : ".$ci->m_transaksi->ambil_jam($key)."<br>";
                         $total_jam++;
                        }
                }
            }?>
        </div>
    </div>
    <div class="row" hidden>
        <div class="col-4">
            List Tanggal Sewa
        </div>
        <div class="col-8">
            <?php      
            $a = array();
            foreach($tanggal_sewa as $v){
                $loop = explode(",",$v->tgl_sewa);
                foreach ($loop as $key => $value) {
                    $a[] = $value;
                }
            }
            $total = count($a);
            for($i =0; $i<$total; $i++){
                echo $a[$i]."<br>";
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Total Hari Tersewa
        </div>
        <div class="col-8">
            <?php echo $total?> hari
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Total Jam Tersewa
        </div>
        <div class="col-8">
            <?php echo $total_jam?> Jam
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Total Harga
        </div>
        <div class="col-8">
            <?php echo $total_harga?>
        </div>
    </div>
</div>

</body>
</html>
<?php }?>