<!DOCTYPE html>
<html>
<?php 
    $this->load->view('admin1/partials/head.php');
?>
	<?php 
	    $ci =& get_instance();
		$ci->load->model('m_transaksi');
	?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
	<?php $this->load->view('admin1/partials/navbar.php') ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo site_url('assets1/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin SISELA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo site_url('assets1/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('admin_nama') ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <?php $this->load->view('admin1/partials/sidebar.php') ?>
      <!-- Sidebar Menu -->
      
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
        </div><!-- /.row -->
          </div><!-- /.col -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- About Me Box -->
            <?php foreach($barang as $data):?>
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><?php echo $data->barang_nama?></h3>
              </div>
              <img class="card-img-top" src="<?php echo base_url('assets/images/barang/'.$data->barang_foto) ?>" alt="Card image" width="100px">
              <div class="card-body">
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <!-- ini buat mahasiswa uad -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Edit Barang</a></li>
                </ul>
              </div><!-- /.card-header -->
            <div class="card-body">
  			<form action="<?php echo site_url('admin1/barang/edit_barang_1')?>" method="POST"  enctype="multipart/form-data">
			<div class="form-group" hidden>
				<label for="usr">Id barang:</label>
				<input type="text" class="form-control" name="xid" value="<?php echo $data->barang_id?>" required>
			</div>
			<div class="form-group">
                <label for="usr">Nama Barang:</label>
                <input type="text" class="form-control" name="xnama" value="<?php echo $data->barang_nama?>" required>
            </div>
            <div class="form-group">
			<label for="usr">Jenis Barang:</label>
                    <?php if($data->barang_tipe_peminjaman == 1){?>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perjam_edit" name="xjenis" value="1" checked>Per Jam
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perhari_edit" name="xjenis" value="2" >Per Hari
                      </label>
                    </div>
                    <?php }else{?>
                      <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perjam_edit" name="xjenis" value="1">Per Jam
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" onclick="javascript:harijam_edit();" id="perhari_edit" name="xjenis" value="2" checked>Per Hari
                      </label>
                    </div>
                    <?php }?>
                    <label for="usr">Harga Sewa:</label>
                    <input type="number" class="form-control" id="usr" name="xharga" value="<?php echo $data->barang_harga?>" required>
			</div>
			<!-- ini untuk jam -->
			<div id="dataperjam_edit" style="display:none">
				<div class="form-group">
					<label for="usr">Minimal Jam sewa:</label>
					<input type="number" class="form-control" id="usr" value="<?php echo $data->barang_min_jam?>" name="xminjam">
				</div>
				<div class="form-group">
					<label for="usr">Maximal Jam sewa:</label>
					<input type="number" class="form-control" id="usr" value="<?php echo $data->barang_max_jam?>" name="xmaxjam">
				</div>
			</div>
			<!-- ini untuk jam -->
			<!-- ini untuk hari -->
			<div id="dataperhari_edit" style="display:none">
				<div class="form-group">
					<label for="usr">Maximal Hari sewa:</label>
					<input type="number" class="form-control" id="usr" value="<?php echo $data->barang_max_hari?>" name="xmaxhari">
				</div>
			</div>
			<!-- ini untuk hari -->
            <div class="form-group">
              <label for="sel1">Kondisi Barang:</label>
              <select class="form-control" id="sel1" name="xkondisi">
                <option value="<?php echo $data->barang_kondisi?>"><?php echo $data->barang_kondisi?></option>
                <option value="Terawat">Terawat</option>
                <option value="TIdak Terawat">TIdak Terawat</option>
              </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan:</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" value="<?php echo $data->barang_keterangan?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Gambar Barang:</label>
                <input type="file" class="form-control-file border" name="filefoto">
			</div>
			<button type="submit" class="btn btn-primary">Simpan</button>
			</form>
                </div><!-- /.card-body -->
			</div>
            <?php endforeach?>
			
            <!-- /.nav-tabs-custom -->
          </div>
            <div class="col-md-9">
            <!-- /.nav-tabs-custom -->
          </div>

          <!-- ini buat mahasiswa uad -->

          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php 
  $this->load->view('admin1/partials/footer.php')
  ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<?php 
   $this->load->view('admin1/partials/js.php');
?>
</body>
<script type="text/javascript">
  function harijam_edit() {
      if (document.getElementById('perhari_edit').checked) {
          document.getElementById('dataperhari_edit').style.display = 'block';
      } else document.getElementById('dataperhari_edit').style.display = 'none';
      if (document.getElementById('perjam_edit').checked) {
          document.getElementById('dataperjam_edit').style.display = 'block';
      } else document.getElementById('dataperjam_edit').style.display = 'none';
  }
</script>

</html>
