<!DOCTYPE html>
<html>
<?php 
    $this->load->view('admin1/partials/head.php');
?>
	<?php 
	    $ci =& get_instance();
		$ci->load->model('m_transaksi');
	?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
	<?php $this->load->view('admin1/partials/navbar.php') ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo site_url('assets1/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin SISELA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo site_url('assets1/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('admin_nama') ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <?php $this->load->view('admin1/partials/sidebar.php') ?>
      <!-- Sidebar Menu -->
      
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
        </div><!-- /.row -->
          </div><!-- /.col -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- About Me Box -->
            <?php foreach($mahasiswa as $data):?>
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><?php echo $data->mahasiswa_nama?></h3>
              </div>
              <img class="card-img-top" src="<?php echo site_url("assets/images/profile/$data->mahasiswa_foto")?>" alt="Card image" height="500px">
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i>No KTP</strong>
                  <p class="text-muted">
                  <?php echo $data->mahasiswa_ktp?>
                  </p>
                <hr>
                <strong><i class="fas fa-map-marker-alt mr-1"></i> NIM/NIP</strong>
                  <p class="text-muted"><?php echo $data->mahasiswa_nim?></p>
                <hr>
                <strong><i class="fas fa-map-marker-alt mr-1"></i> Institusi</strong>
                  <p class="text-muted"><?php echo $data->mahasiswa_instansi?></p>
                <hr>
                <strong><i class="fas fa-map-marker-alt mr-1"></i> Jenis Kelamin</strong>
                  <p class="text-muted"><?php echo $data->mahasiswa_jk?></p>
                <hr>
                <strong><i class="fas fa-map-marker-alt mr-1"></i> Email</strong>
                  <p class="text-muted"><?php echo $data->mahasiswa_email?></p>
                <hr>
                <strong><i class="fas fa-map-marker-alt mr-1"></i> No Hp</strong>
                  <p class="text-muted"><?php echo $data->mahasiswa_nohp?></p>
                <hr>
              </div>
              <!-- /.card-body -->
            </div>
            <?php endforeach?>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <!-- ini buat mahasiswa uad -->
          <?php if($status == 1){ ?>
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
								<?php if($status_pengguna == 0 ){?>
                <!-- ini untuk pengguna yang masih aktif -->
									<div class="alert alert-info">
                    <strong>STATUS : PENGGUNA AKTIF</strong>
									</div>								
								<!-- ini untuk pengguna yang masih aktif -->
								<?php }elseif($status_pengguna == 1){?>
								<!-- ini untuk pengguna yang masih aktif -->
									<div class="alert alert-danger">
											<strong>STATUS : PENGGUNA NON-AKTIF</strong>
									</div>
								<!-- ini untuk pengguna yang masih aktif -->
								<?php }?>
              </div><!-- /.card-header -->
              <?php if($verifikasi == 0 ){?>
              <div class="card-body">
              <div class="alert alert-danger">
                <strong>Belum Upload File</strong>
              </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
              <?php }elseif($verifikasi == 1 ){?>
                <?php foreach($mahasiswa as $data):?>
                  <div class="card-body">
                    <div class="tab-content">
                      <div class="active tab-pane" id="activity">
                        <!-- Post -->
                        <div class="tab-pane" id="settings">
                        <form action="<?php echo site_url('admin1/mahasiswa/verifikasi_mahasiswa')?>" method="POST">
                          <div class="form-group row" hidden>
                            <label for="inputName" class="col-sm-2 col-form-label">Id Mahasiswa</label>
                            <div class="col-sm-10">
                              <input type="text" name="xid" value="<?php echo $id?>">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">Skripsi 2 / Surat Terlibat Penelitian Dosen</label>
                            <div class="col-sm-10">
                              <a href="<?php echo site_url("assets/file/uad/$data->file_skripsi") ?>" class="btn btn-success col-12" role="button">Download</a>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label">Bukti Transfer</label>
                            <div class="col-sm-10">
                              <a href="<?php echo site_url("assets/file/uad/$data->file_bukti_transfer") ?>" class="btn btn-success col-12" role="button">Download</a>
                            </div>
                          </div>
                          <div class="container">
                            <div class="row">
                              <div class="col-6">
                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Tolak</button>
                                  <button type="submit" class="btn btn-info">Verifikasi</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                        <!-- /.post -->
                      </div>
                      <!-- /.tab-pane -->


                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div><!-- /.card-body -->
                <?php endforeach?>
              <?php }elseif($verifikasi == 2 ){?>
                <div class="card-body">
                  <div class="alert alert-success">
                    <strong>Sudah Terverifikasi</strong>
                  </div>
                      <!-- widget buat banyak kali pesan -->
                      <div class="row">
                          <div class="col-6">
                            <div class="info-box">
                              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Penyewaan</span>
                                <span class="info-box-number"><?php echo $total_penyewaan?></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="info-box">
                              <span class="info-box-icon bg-warning"><i class="far fa-envelope"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Biaya</span>
                                <span class="info-box-number"><?php echo $total_biaya?></span>
                              </div>
                            </div>
                          </div>
                      </div>
                      <!-- widget buat banyak kali pesan -->
                      <!-- widget buat riwayat -->
                      <div class="row">
                          <div class="col-12">
                            <div class="card card-info">
                              <div class="card-header">
                                <h3 class="card-title">Riwayat Penyewaan</h3>

                                <div class="card-tools">
                                  <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/pages/widgets.html" data-source-selector="#card-refresh-content"><i class="fas fa-sync-alt"></i></button>
                                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                              </div>
                              <div class="card-body">
																<div class="card-body table-responsive p-0" style="height: 600px;">
																	<table class="table table-head-fixed">
																		<thead>
																			<tr>
																				<th>No</th>
																				<th>Nama Barang</th>
																				<th>Tanggal Sewa</th>
																				<th>Lama Sewa</th>
																				<th>Biaya</th>
																				<th>Alasan</th>
																				<th>Status</th>
																			</tr>
																		</thead>
																		<tbody>
																			<?php $no = 0; foreach($riwayat as $data): $no++; $durasi = explode(",", $data->durasi_jam);?>
																			<?php $tanggal = substr($data->tgl_sewa, 0, 10);?>
																			<tr>
																				<td><?php echo $no?></td>
																				<td><?php echo $data->barang_nama?></td>
																				<td><?php echo $tanggal?></td>
																				<td>
																					<?php if($data->tipe_peminjaman==2){?>
																						<div class="form-group">
																							<span><?php echo $data->lama_sewa." Hari"?></span>
																						</div>
																				<?php }elseif($data->tipe_peminjaman==1){?>
																					<span>
																						<?php foreach($durasi as $key):?>
																							<?php echo "Jam ke ".$key." : ".$ci->m_transaksi->ambil_jam($key)."<br>"?>
																							<?php endforeach?>
																						</span>
																						<?php }?>
																					</td>
																				<td>
																						<?php echo $data->total?>
																				</td>
																				<td>
																					<?php echo $data->tujuan_penyewaan?>
																				</td>
																				<td>
																						<?php if($data->status_transaksi==1){
																							echo "Menunggu Konfirmasi";
																						}elseif($data->status_transaksi==2){
																							echo "Diterima";
																						}elseif($data->status_transaksi==3){
																							echo "Ditolak";
																						}elseif($data->status_transaksi==4){
																							echo "Batal";
																						}?>
																				</td>
																			</tr>
																			<?php endforeach?>
																		</tbody>
																	</table>
																</div>
															</div>
															<div class="card-footer">
																<?php if($status_pengguna == 0){?>
																	<!-- non aktifkan mahasiswa -->
																	<a href="<?php echo site_url("admin1/mahasiswa/non_aktifkan/$id")?>" type="button" class="btn btn-block btn-danger btn-sm">Non-aktifkan Mahasiswa</a>
																	<!-- non aktifkan mahasiswa -->
																<?php }elseif($status_pengguna == 1){?>
																	<!-- Aktifkan mahasiswa -->
																	<a href="<?php echo site_url("admin1/mahasiswa/re_aktifkan/$id")?>" type="button" class="btn btn-block btn-info btn-sm">Aktifkan Kembali Mahasiswa</a>
																	<!-- Aktifkan mahasiswa -->
																<?php }?>
															</div>
                            </div>
                          </div>
                      </div>
                      <!-- widget buat riwayat -->


                </div><!-- /.card-body -->
              <?php }?>
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <?php }else{?>
            <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
								<?php if($status_pengguna == 0 ){?>
                <!-- ini untuk pengguna yang masih aktif -->
									<div class="alert alert-info">
                    <strong>STATUS : PENGGUNA AKTIF</strong>
									</div>								
								<!-- ini untuk pengguna yang masih aktif -->
								<?php }elseif($status_pengguna == 1){?>
								<!-- ini untuk pengguna yang masih aktif -->
									<div class="alert alert-danger">
											<strong>STATUS : PENGGUNA NON-AKTIF</strong>
									</div>
								<!-- ini untuk pengguna yang masih aktif -->
								<?php }?>
              </div><!-- /.card-header -->
              <?php if($verifikasi == 0 ){?>
              <div class="card-body">
              <div class="alert alert-danger">
                <strong>Belum Upload File</strong>
              </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
              <?php }elseif($verifikasi == 1 ){?>
                <?php foreach($mahasiswa as $data):?>
                  <div class="card-body">
                    <div class="tab-content">
                      <div class="active tab-pane" id="activity">
                        <!-- Post -->
                        <div class="tab-pane" id="settings">
                        <form action="<?php echo site_url('admin1/mahasiswa/verifikasi_mahasiswa')?>" method="POST">
                          <div class="form-group row" hidden>
                            <label for="inputName" class="col-sm-2 col-form-label">Id Mahasiswa</label>
                            <div class="col-sm-10">
                              <input type="text" name="xid" value="<?php echo $id?>">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">Surat Izin Pemakaian Lab</label>
                            <div class="col-sm-10">
                              <a href="<?php echo site_url("assets/file/luar/$data->file_surat_izin_lab") ?>" class="btn btn-success col-12" role="button">Download</a>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label">Bukti Transfer</label>
                            <div class="col-sm-10">
                              <a href="<?php echo site_url("assets/file/luar/$data->file_bukti_transfer") ?>" class="btn btn-success col-12" role="button">Download</a>
                            </div>
                            <div class="form-group row">
                          </div>
                            <label for="inputEmail" class="col-sm-2 col-form-label">KTP/SIM/PASSPORT</label>
                            <div class="col-sm-10">
                              <a href="<?php echo site_url("assets/file/luar/$data->file_ktp") ?>" class="btn btn-success col-12" role="button">Download</a>
                            </div>
                          </div>
                          <div class="container">
                            <div class="row">
                              <div class="col-6">
                                  <a href="<?php echo site_url("admin1/mahasiswa/tolak_verifikasi/$id")?>" type="button" class="btn btn-danger">Tolak</a>
                                  <button type="submit" class="btn btn-info">Verifikasi</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                        <!-- /.post -->
                      </div>
                      <!-- /.tab-pane -->


                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div><!-- /.card-body -->
                <?php endforeach?>
              <?php }elseif($verifikasi == 2 ){?>
                <div class="card-body">
                  <div class="alert alert-success">
                    <strong>Sudah Terverifikasi</strong>
                  </div>
                  <div class="row">
                          <div class="col-6">
                            <div class="info-box">
                              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Penyewaan</span>
                                <span class="info-box-number"><?php echo $total_penyewaan?></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="info-box">
                              <span class="info-box-icon bg-warning"><i class="far fa-envelope"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Biaya</span>
                                <span class="info-box-number"><?php echo $total_biaya?></span>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-12">
                            <div class="card card-info">
                              <div class="card-header">
                                <h3 class="card-title">Riwayat Penyewaan</h3>

                                <div class="card-tools">
                                  <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/pages/widgets.html" data-source-selector="#card-refresh-content"><i class="fas fa-sync-alt"></i></button>
                                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                              </div>
                              <div class="card-body">
                              <div class="card-body table-responsive p-0" style="height: 600px;">
                                <table class="table table-head-fixed">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Nama Barang</th>
                                      <th>Tanggal Sewa</th>
                                      <th>Lama Sewa</th>
                                      <th>Biaya</th>
                                      <th>Alasan</th>
                                      <th>Status</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $no = 0; foreach($riwayat as $data): $no++; $durasi = explode(",", $data->durasi_jam);?>
                                    <?php $tanggal = substr($data->tgl_sewa, 0, 10);?>
                                    <tr>
                                      <td><?php echo $no?></td>
                                      <td><?php echo $data->barang_nama?></td>
                                      <td><?php echo $tanggal?></td>
                                      <td>
                                        <?php if($data->tipe_peminjaman==2){?>
                                          <div class="form-group">
                                            <span><?php echo $data->lama_sewa." Hari"?></span>
                                          </div>
                                      <?php }elseif($data->tipe_peminjaman==1){?>
                                        <span>
                                          <?php foreach($durasi as $key):?>
                                            <?php echo "Jam ke ".$key." : ".$ci->m_transaksi->ambil_jam($key)."<br>"?>
                                            <?php endforeach?>
                                          </span>
                                          <?php }?>
                                        </td>
                                      <td>
                                          <?php echo $data->total?>
                                      </td>
                                      <td>
                                        <?php echo $data->tujuan_penyewaan?>
                                      </td>
                                      <td>
                                          <?php if($data->status_transaksi==1){
                                            echo "Menunggu Konfirmasi";
                                          }elseif($data->status_transaksi==2){
                                            echo "Diterima";
                                          }elseif($data->status_transaksi==3){
                                            echo "Ditolak";
                                          }elseif($data->status_transaksi==4){
                                            echo "Batal";
                                          }?>
                                      </td>
                                    </tr>
                                    <?php endforeach?>
                                  </tbody>
                                </table>
                              </div>
															</div>
															<div class="card-footer">
																<?php if($status_pengguna == 0){?>
																	<!-- non aktifkan mahasiswa -->
																	<a href="<?php echo site_url("admin1/mahasiswa/non_aktifkan/$id")?>" type="button" class="btn btn-block btn-danger btn-sm">Non-aktifkan Mahasiswa</a>
																	<!-- non aktifkan mahasiswa -->
																<?php }elseif($status_pengguna == 1){?>
																	<!-- Aktifkan mahasiswa -->
																	<a href="<?php echo site_url("admin1/mahasiswa/re_aktifkan/$id")?>" type="button" class="btn btn-block btn-info btn-sm">Aktifkan Kembali Mahasiswa</a>
																	<!-- Aktifkan mahasiswa -->
																<?php }?>

															</div>
                            </div>
                          </div>
                      </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              <?php }?>
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <?php }?>

          <!-- ini buat mahasiswa uad -->

          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php 
  $this->load->view('admin1/partials/footer.php')
  ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<?php 
   $this->load->view('admin1/partials/js.php');
?>
</body>
</html>

<!-- The Modal -->
<?php foreach($mahasiswa as $data):?>
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tolak Verifikasi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Tolak terverifikasi data <strong><?php echo $data->mahasiswa_nama?></strong>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a href="<?php echo site_url("admin1/mahasiswa/tolak_verifikasi/$data->mahasiswa_id")?>" class="btn btn-danger">Tolak</a>
      </div>
    </div>
  </div>
</div>
<?php endforeach?>
