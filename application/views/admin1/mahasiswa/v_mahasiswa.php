<!DOCTYPE html>
<html>
<?php 
    $this->load->view('admin1/partials/head.php');
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
	<?php $this->load->view('admin1/partials/navbar.php') ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo site_url('assets1/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin SISELA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo site_url('assets1/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('admin_nama') ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <?php $this->load->view('admin1/partials/sidebar.php') ?>
      <!-- Sidebar Menu -->
      
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Pengguna</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Mahasiswa</th>
                  <th>NIM/NIP</th>
                  <th>No Ktp</th>
                  <th>Jenis Kelamin</th>
                  <th>No Hp</th>
                  <th>Instansi</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0; foreach($mahasiswa as $data): $no++;?>
                <tr>
                  <td><?php echo $no?></td>
                  <td><?php echo $data->mahasiswa_nama?></td>
                  <td><?php echo $data->mahasiswa_nim?></td>
                  <td><?php echo $data->mahasiswa_ktp?></td>
                  <td><?php echo $data->mahasiswa_jk?></td>
                  <td><?php echo $data->mahasiswa_nohp?></td>
                  <td><?php echo $data->mahasiswa_instansi?></td>
                  <td><?php echo $data->mahasiswa_email?></td>
                  <td>
                      <?php if($data->mahasiswa_verifikasi == 0){
                                echo "Belum Kirim Berkas";
                            }elseif($data->mahasiswa_verifikasi == 1){
                                echo "Menunggu Verifikasi";
                            }elseif($data->mahasiswa_verifikasi == 2){
                                echo "Sudah Terverifikasi";
                            }
                      ?>
                  </td>
                  <td>
                    <a href="<?php echo site_url("admin1/mahasiswa/detail_mahasiswa/$data->mahasiswa_id")?>" class="btn btn-block bg-gradient-primary">Detail</a>
                  </td>
                </tr>
                <?php endforeach?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('admin1/partials/footer.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<?php 
    $this->load->view('admin1/partials/js.php');
?>
</body>
</html>
