<?php 
$konek = mysqli_connect('localhost','root','','db_farmasi');
$datamahasiswa = mysqli_query($konek, "SELECT COUNT(*) FROM tbl_mahasiswa WHERE mahasiswa_verifikasi=0 OR mahasiswa_verifikasi=1");
$nilainya = mysqli_fetch_row($datamahasiswa);
$datatransaksi = mysqli_query($konek, "SELECT COUNT(*) FROM tbl_transaksi WHERE status_transaksi=1");
$banyak_transaksi = mysqli_fetch_row($datatransaksi);
$datatransaksiraungan = mysqli_query($konek, "SELECT COUNT(*) FROM tbl_transaksi_ruangan WHERE status_transaksi=1");
$banyak_transaksiruangan = mysqli_fetch_row($datatransaksiraungan);

?>
<!-- ini untuk admin alat -->
<?php if($this->session->userdata('status_admin') == 1){?>
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?php echo site_url('admin')?>" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url('admin1/mahasiswa')?>" class="nav-link">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                Mahasiswa
                <?php if($nilainya[0] != 0){?>
                <span class="right badge badge-danger">New</span>
                <?php }?>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-mortar-pestle"></i>
              <p>
                Alat Praktikum
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/barang')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Alat</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-exchange-alt"></i>
              <p>
                Transaksi Alat
                <i class="fas fa-angle-left right"></i>
                <?php if($banyak_transaksi[0] != 0){?>
                <span class="right badge badge-danger"><?php echo $banyak_transaksi[0]?></span>
                <?php }?>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/transaksi')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Masuk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/transaksi/transaksi_diterima')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Diterima</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/transaksi/transaksi_ditolak')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Ditolak</p>
                </a>
              </li>
            </ul>
            </li>
          <li class="nav-item">
            <a href="<?php echo site_url('admin1/login/user_logout')?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Log out
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
<?php }elseif($this->session->userdata('status_admin') == 2){?>
    <!-- ini untuk admin alat -->
<!-- ini untuk admin monitoring -->
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?php echo site_url('admin')?>" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-exchange-alt"></i>
              <p>
                Transaksi Alat
                <i class="fas fa-angle-left right"></i>
                <?php if($banyak_transaksi[0] != 0){?>
                <span class="right badge badge-danger"><?php echo $banyak_transaksi[0]?></span>
                <?php }?>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/transaksi/transaksi_diterima')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Diterima</p>
                </a>
              </li>
            </ul>
            <li class="nav-item">
            <a href="<?php echo site_url('admin1/login/user_logout')?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Log out
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
<?php }elseif($this->session->userdata('status_admin') == 3){?>
<!-- ini untuk admin monitoring -->
<!-- ini untuk admin ruangan -->
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?php echo site_url('admin')?>" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
              <a href="<?php echo site_url('admin1/ruangan')?>" class="nav-link">
                <i class="nav-icon fas fa-home"></i>
                <p>
                  Ruangan
                </p>
              </a>
          </li>
          <li class="nav-item has-treeview">
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-exchange-alt"></i>
              <p>
                Transaksi Ruangan
                <i class="fas fa-angle-left right"></i>
                <?php if($banyak_transaksiruangan[0] != 0){?>
                <span class="right badge badge-danger"><?php echo $banyak_transaksiruangan[0]?></span>
                <?php }?>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/transaksi/transaksi_masuk_ruangan')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Masuk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/transaksi/transaksi_diterima_ruangan')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Diterima</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('admin1/transaksi/transaksi_ditolak_ruangan')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Transaksi Ditolak</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url('admin1/login/user_logout')?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Log out
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
<?php }?>
<!-- ini untuk admin ruangan -->
