<!DOCTYPE html>
<html>
<?php 
    $this->load->view('admin1/partials/head.php');
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
	<?php $this->load->view('admin1/partials/navbar.php') ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo site_url('assets1/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin SISELA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo site_url('assets1/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('admin_nama') ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <?php $this->load->view('admin1/partials/sidebar.php') ?>
      <!-- Sidebar Menu -->
      
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Transaksi Diterima</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>No</th>
                    <th>NIM/NIP</th>
                    <th>Nama Penyewa</th>
                    <th>Nama Barang</th>
                    <th>Tanggal Sewa</th>
                    <th>Harga Sewa</th>
                    <th>sampel</th>
                    <th>Kode Penyewaan</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0; foreach($transaksi as $data): $no++?>
							  <?php $tanggal = substr($data->tgl_sewa, 0, 10);?>  
                  <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->mahasiswa_nim?></td>
                      <td><?php echo $data->mahasiswa_nama?></td>
                      <td><?php echo $data->barang_nama?></td>
                      <td><?php echo $tanggal?></td>
                      <td><?php echo $data->total?></td>
                      <td><?php echo $data->sampel?></td>
                      <td><?php echo $data->kode_penyewaan?></td>
                    </tr>
                <?php endforeach?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('admin1/partials/footer.php')?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<?php 
    $this->load->view('admin1/partials/js.php');
?>
</body>
</html>
   <!-- ============================= Edit ========================= -->
   <?php foreach($transaksi as $data):?>
  <div class="modal fade" id="modelkonfirmasi<?php echo $data->kode_penyewaan?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cek Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for="usr">kode penyewaan:</label>
                <input type="text" class="form-control" name="xid" value="<?php echo $data->kode_penyewaan?>" readonly required>
            </div>
            <div class="form-group">
                <label for="usr">Nama Mahasiswa:</label><br>
                <span><?php echo $data->mahasiswa_nama?></span>
            </div>
            <hr>
            <div class="form-group">
                <label for="usr">Nama Barang:</label><br>
                <span><?php echo $data->barang_nama?></span>
            </div>
            <hr>
            <div class="form-group">
                <label for="usr">Lama penyewaan:</label><br>
                <span><?php echo $data->lama_sewa." Hari"?></span>
            </div>
            <hr>
            <div class="form-group">
                <label for="usr">Tujuan Penyewaan:</label><br>
                <span><?php echo $data->tujuan_penyewaan?></span>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
      </div>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->
