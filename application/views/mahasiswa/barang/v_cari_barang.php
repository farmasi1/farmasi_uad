<!DOCTYPE html>
<html lang="en">
<?php 
      $this->load->view("mahasiswa/partial/head.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_styles.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_responsive.css')?>">
<body>
<style>
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

}

div.container {
  padding: 10px;
}
</style>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
        <div class="home_background" style="background-image:url(<?php echo site_url('images/home/fhome2.png')?>)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						
						<!-- Main Navigation -->

                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
	</header>

	<!-- Listings -->

	<div class="listings" id="grad1">
		<div class="container">
        <div class="container" style="margin-bottom:25px;">
			<form action="<?php echo site_url('mahasiswa/barang/cari_barang') ?>" method="POST">
				<div class="row" style="padding: 20px;">
					<div class="col-12">
						<div class="wrap">
						<div class="search">
								<input type="text" class="searchTerm" name="xsearch" placeholder="Cari alat praktikum">
								<button type="submit" class="searchButton">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			</div>
			<div class="row">
				<!-- Listings -->
				<div class="col-lg-12">
                    <!-- listing item bs -->
                    <div class="row">
    					<?php foreach($caribarang as $data): ?>
                            <div class="col-3">
                                <div class="card polaroid">
                                    <img class="card-img-top" src="<?php echo site_url('assets/images/barang/'.$data->barang_foto)?>" alt="Card image" style="width:100%">
                                    <div class="card-body">
                                    <h4 class="card-title" style="color: black;"><strong><?php echo $data->barang_nama?></strong></h4>
                                    <span style="color: black"><?php echo $data->barang_keterangan?></span>
									<?php if($data->barang_tipe_peminjaman==1){?>
										<div class="featured_card_price_title">Harga Sewa / Jam : <br> Rp <?php echo $data->barang_harga?></div>
									<?php }else{?>
										<div class="featured_card_price_title">Harga Sewa / Hari <br> Rp <?php echo $data->barang_harga?></div>
									<?php }?>
                                    <a href="<?php echo site_url("mahasiswa/barang/detail_barang/$data->barang_id")?>" class="btn btn-primary">Detail</a>
                                    </div>
                                </div>
                            </div> 
        				<?php endforeach?>
                    </div>
                    <!-- listing item bs -->

				</div>

			</div>

			<!-- <div class="row">
				<div class="col clearfix">
					<div class="listings_nav">
						<ul>
							<li class="listings_nav_item active"><a href="#">01.</a></li>
							<li class="listings_nav_item"><a href="#">02.</a></li>
							<li class="listings_nav_item"><a href="#">03.</a></li>
						</ul>
					</div>
				</div>
			</div> -->
		</div>
	</div>

	<!-- Newsletter -->

		<!-- ini buat footer -->
        <?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->

</div>

<!-- ini buat js -->
<?php 
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->

</body>

</html>
