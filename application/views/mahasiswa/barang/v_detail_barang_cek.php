<?php 
// ini buat menghitung tanggal yang udah di booking
if($tanggal != null){

  foreach($tanggal as $v){
    $loop = explode(",",$v->tgl_sewa);
    foreach ($loop as $key => $value) {
      $a[] = $value;
    }
  }
  $batas = count($a);
  for($i =0; $i<$batas; $i++){
    $a[$i]."<br>";
  }
}else{
  $batas = 1;
  $a = array( "2019-13-01");
}
  // ini buat menghitung tanggal yang udah di booking

?>
<!DOCTYPE html>
<html lang="en">
<?php 
      $this->load->view("mahasiswa/partial/head.php");
?>
<head>
  <link rel="stylesheet" href="<?php echo site_url('assets/kalender/assets/style.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_styles.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_responsive.css')?>">
  <link rel="stylesheet" href="<?php echo site_url('assets/kalender/assets/dateTimePicker.css')?>">
</head>
<style>
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

}

div.container {
  padding: 10px;
}
</style><style>
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

}

div.container {
  padding: 10px;
}
</style>


<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
        <div class="home_background" style="background-image:url(<?php echo site_url('images/farmasi3.jpg')?>)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						
						<!-- Main Navigation -->

                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
	</header>

	<!-- Listings -->

	<div class="listings">
  <div class="container">
                          <center>
                        <div class="alert alert-info">
                            Penyewaan Barang
                        </div>
                    </center>

    </div>
		<div class="container polaroid">
			<div class="row">
				<!-- Listings -->
        <?php if($status_barang == 2){?>
				<div class="col-lg-12">
                    <!-- listing item bs -->
                    <div class="row">
              					<?php foreach($barang as $data): ?>
                            <div class="col-3">
                                <div class="card" style="padding-bottom:30px;">
                                    <img class="card-img-top" src="<?php echo site_url('assets/images/barang/'.$data->barang_foto)?>" alt="Card image" style="width:100%">
                                    <div class="card-body">
                                    <h4 class="card-title"><strong><?php echo $data->barang_nama?></strong></h4>
                                    <p class="card-text"><?php echo $data->barang_keterangan?></p>
                                    <p class="card-text">Harga Sewa / 24 jam : <br> Rp <?php echo $data->barang_harga?></p>
                                    </div>
                                </div>
                            </div> 
                        <?php endforeach?>
                        <div class="col-6">
                            <div class="container">
                              <div id="glob-data" data-toggle="calendar"></div>
                              <div>
                        <br>
                              <form action="<?php echo site_url('mahasiswa/transaksi/simpan_transaksi_perhari')?>" method="POST">
                              <div class="form-group" hidden>
                                <label for="email">Id Peminjamn:</label>
                                <input type="number" class="form-control col-3" name="xnim" value="<?php echo $this->session->userdata('mahasiswa_nim')?>" readonly>
                              </div>
                              <div class="form-group">
                                <label for="email">Tanggal Sewa:</label>
                                <input type="text" class="form-control col-3" name="xtanggal" id="tanggalku" readonly required>
                              </div>
                              <div class="form-group" hidden>
                                <label for="email">barang id:</label>
                                <input type="number" class="form-control col-3" placeholder="Pilih Dari Kalender" name="xbarangid" value="<?php echo $barang_id?>"readonly required>
                              </div>
                              <div class="form-group">
                                <label for="sel1">Lama Peminjaman</label>
                                <select class="form-control col-3" id="sel1" name="xlama" required>
                                  <option value="1">1 Hari</option>
                                  <option value="2">2 Hari</option>
                                  <option value="3">3 Hari</option>
                                  <option value="4">4 Hari</option>
                                  <option value="5">5 Hari</option>
                                  <option value="6">6 Hari</option>
                                  <option value="7">7 Hari</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="email">Alasan Peminjaman</label>
                                <input type="text" class="form-control col-3" placeholder="Alasan Peminjaman" name="xtujuan" required>
                              </div>
                              <button type="submit" class="btn btn-primary">Kirim</button>
                              </form>
                              </div>
                            </div>
                        </div>
                    </div>
                    <!-- listing item bs -->
				</div>
        <?php }elseif($status_barang == 1){?>
          <div class="col-lg-12">
                    <!-- listing item bs -->
                    <div class="row">
              					<?php foreach($barang as $data): ?>
                            <div class="col-3">
                                <div class="card" style="padding-bottom:30px;">
                                    <img class="card-img-top" src="<?php echo site_url('assets/images/barang/'.$data->barang_foto)?>" alt="Card image" style="width:100%">
                                    <div class="card-body">
                                    <h4 class="card-title"><strong><?php echo $data->barang_nama?></strong></h4>
                                    <p class="card-text"><?php echo $data->barang_keterangan?></p>
                                    <p class="card-text">Harga Sewa / 24 jam : <br> Rp <?php echo $data->barang_harga?></p>
                                    </div>
                                </div>
                            </div> 
                        <?php endforeach?>
                        <div class="col-6">
                          <form action="<?php echo site_url('mahasiswa/barang/cek_jadwal_peminjaman_jam')?>" method="POST">
                                
                              <div class="form-group">
                              <div class="alert alert-success alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <?php foreach($barang as $data){
                                    echo "Untuk barang ".$data->barang_nama." minimal peminjaman selama ".$data->barang_min_jam." jam dan maksimal peminjaman selama ".$data->barang_max_jam." jam.";
                                  }?>
                                </div>  
                              </div>
                              <div class="form-group">
                                <label for="email">Tanggal Sewa:</label>
                                <input type="date" class="form-control col-12" value="<?php echo $tanggal_sewa?>"name="xtanggal" onchange="this.form.submit();">
                              </div>
                              <div class="form-group" hidden>
                                <label for="email">barang id:</label>
                                <input type="number" class="form-control col-12" placeholder="Pilih Dari Kalender" name="xbarangid" value="<?php echo $barang_id?>"readonly required>
                              </div>
                          </form>
                              <form action="<?php echo site_url('mahasiswa/transaksi/simpan_transaksi_perjam')?>" method="POST">
                              <div class="form-group" hidden>
                                <label for="email">Id Peminjamn:</label>
                                <input type="number" class="form-control col-12" name="xnim" value="<?php echo $this->session->userdata('mahasiswa_nim')?>" readonly>
                              </div>
                              
                              <div class="form-group" hidden>
                                <label for="email">barang id:</label>
                                <input type="number" class="form-control col-12" placeholder="Pilih Dari Kalender" name="xbarangid" value="<?php echo $barang_id?>"readonly required>
                              </div>
                              <div class="form-group" hidden>
                                <label for="email">Tanggal Sewa:</label>
                                <input type="date" class="form-control col-12" placeholder="Pilih Dari Kalender" name="xtanggal" value="<?php echo $tanggal_sewa?>">
                              </div>
                              <div class="form-group">
                                <label for="email">Jam Penyewaan</label>
                                <div class="row">
                                  <?php foreach($jam as $data):?>
                                    <div class="col-12">
                                      <label class="form-check-label">
                                        <?php if(in_array($data->jam_sesi,$jam_terpakai)){?>
                                          <input type="checkbox" class="form-check-input" name="xdurasi[]" value="<?php echo $data->jam_sesi?>" disabled  ><span class="badge badge-danger" style="padding-left:35px;"><?php echo $data->jam_waktu?></span>
                                        <?php }else{?>
                                          <input type="checkbox" class="jam" name="xdurasi[]" value="<?php echo $data->jam_sesi?>"><span style="padding-left:20px;"><?php echo $data->jam_waktu?></span>
                                        <?php }?>
                                      </label>
                                    </div>
                                  <?php endforeach ?>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="email">Sampel:</label>
                                <input type="text" class="form-control col-12" name="xsampel" required>
                              </div>
                              <div class="form-group">
                                <label for="email">Alasan Peminjaman</label>
                                <div class="form-check">
                                  <label class="form-check-label" for="radio1">
                                    <input type="radio" class="form-check-input" id="radio1" name="xtujuan" value="praktikum" checked>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Praktikum
                                  </label>
                                </div>
                                <div class="form-check">
                                  <label class="form-check-label" for="radio2">
                                    <input type="radio" class="form-check-input" id="radio2" name="xtujuan" value="penelitian">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penelitian
                                  </label>
                                </div>
                              </div>
                              <button type="submit" class="btn btn-primary col-12">Kirim</button>
                              </form>
                              </div>
                            </div>
                        </div>
                    </div>
                    <!-- listing item bs -->
				  </div>
        <?php }?>

			</div>
		</div>
	</div>

	<!-- Newsletter -->

		<!-- ini buat footer -->
        <?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->

</div>

<!-- ini buat js -->
<?php 
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->

</body>

</html>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="<?php echo site_url('assets/kalender/assets/style.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/kalender/assets/dateTimePicker.css')?>">

  </head>
  <body>
  				
			</pre>
        </div>
      </div>
      
  </div>
    <script type="text/javascript" src="<?php echo site_url('assets/kalender/scripts/components/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('assets/kalender/scripts/dateTimePicker.min.js') ?>"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {
      $('#basic').calendar();
      $('#glob-data').calendar(
      {
        unavailable: [
          <?php for($i =0; $i<$batas; $i++){?>
          '<?php echo $a[$i]?>',
          <?php }?>
        ],
        onSelectDate: function(date, month, year)
        {
          var cinta = [year, month, date].join('-'); 
          // alert(cinta + ' is: ' + this.isAvailable(date, month, year));
          document.getElementById("tanggalku").value = cinta;
        }
      });
      $('#show-next-month').calendar(
      {
        num_next_month: 1,
        num_prev_month: 1,
        unavailable: ['*-*-9', '*-*-10']
      });
    });
    $.ajax( {
    type: 'GET',
    data: "&val=" + cinta, 
    success: function(data) {
        alert("data")
    }
    } );
    </script>
    <script>
      var max_limit = <?php echo $max_jam?> // Max Limit
      $(document).ready(function (){
          $(".jam:input:checkbox").each(function (index){
              this.checked = (".jam:input:checkbox" < max_limit);
          }).change(function (){
              if ($(".jam:input:checkbox:checked").length > max_limit){
                  this.checked = false;
              }
          });
      });
    </script>
  </body>
</html>
