<!DOCTYPE html>
<html lang="en">
<?php 
      $this->load->view("mahasiswa/partial/head.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_styles.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_responsive.css')?>">


<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
        <div class="home_background" style="background-image:url(<?php echo site_url('images/home/fhome2.png')?>)"></div>
        <div class="home_slider_content_container text-center">
						<div class="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Daftar Akun </h1>
						</div>
					</div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						
						<!-- Main Navigation -->

                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
	</header>

	<!-- Listings -->

	<div class="listings">
		<div class="container">
			<div class="row">
				<!-- Listings -->
				<div class="col-lg-12">
                    <div class="card" style="width:100%">
                        <div class="card-body">
                        <form action="<?php echo site_url('mahasiswa/login/daftar_akun')?>" method="POST" enctype="multipart/form-data">
                            <div class="container">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Nama Lengkap</label>
                                            <input type="text" class="form-control" id="usr" name="xnama"required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">NIM / NIP </label>
                                            <input type="number" class="form-control" id="usr" name="xnim"required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Jenis Kelamin</label>
                                            <br>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="xjk" value="pria">Laki - Laki
                                                </label>
                                                </div>
                                                <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="xjk" value="wanita">Perempuan
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Mahasiswa UAD.?</label>
                                            <br>
                                            <div class="form-check-inline">
                                                <label class="form-check-label" style="color:black">
                                                    <input type="radio" class="form-check-input" name="xstatus" value="1">Iya
                                                </label>
                                                </div>
                                                <div class="form-check-inline">
                                                <label class="form-check-label" style="color:black">
                                                    <input type="radio" class="form-check-input" name="xstatus" value="0">Tidak
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Nama Instansi</label>
                                            <input type="text" class="form-control" id="usr" name="xinstansi"required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">No KTP</label>
                                            <input type="number" class="form-control" id="usr" name="xktp"required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">No Handphone</label>
                                            <input type="number" class="form-control" id="usr" name="xnohp"required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Alamat Email</label>
                                            <input type="email" class="form-control" id="usr" name="xemail" required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Password</label>
                                            <input type="password" class="form-control" id="usr" name="xpassword" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Foto Profile</label>
                                            <input type="file" class="form-control-file border" name="filefoto" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-primary">Daftar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>

				</div>

			</div>
		</div>
	</div>

	<!-- Newsletter -->

		<!-- ini buat footer -->
        <?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->

</div>

<!-- ini buat js -->
<?php 
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->

</body>

</html>
