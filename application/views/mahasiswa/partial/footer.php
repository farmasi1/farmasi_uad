<div class="testimonials">
		<div class="testimonials_background_container prlx_parent">
			<div class="testimonials_background prlx" style="background-image:url(images/testimonials_background.jpg)"></div>
		</div>
		<div class="container">

			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h3>Sambutan Civitas Fakultas Farmasi</h3>
						<span class="section_subtitle">Universitas Ahmad DAhlan</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					
					<div class="testimonials_slider_container">

						<!-- Testimonials Slider -->
						<div class="owl-carousel owl-theme testimonials_slider">
							
							<!-- Testimonials Item -->
							<div class="owl-item">
								<div class="testimonials_item text-center">
									<p class="testimonials_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae neque libero. Vivamus vel interdum massa. Mauris ut felis vel diam pretium eleifend vel eu neque. Mauris a condimentum tortor. Cras nec molestie est. Nulla vel facilisis metus. Quisque tempus fermentum enim, in feugiat sem laoreet</p>
									<div class="testimonial_user">
										<div class="testimonial_image mx-auto">
											<img src="<?php echo site_url('assets/images/staff/1.png')?>" alt="https://unsplash.com/@remdesigns">
										</div>
										<div class="testimonial_name">Dr. Dyah Aryani Perwitasari, Ph.D., Apt</div>
										<div class="testimonial_title">Dekan Fakultas Farmasi UAD</div>
									</div>
								</div>
							</div>

							<!-- Testimonials Item -->
							<div class="owl-item">
								<div class="testimonials_item text-center">
									<p class="testimonials_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae neque libero. Vivamus vel interdum massa. Mauris ut felis vel diam pretium eleifend vel eu neque. Mauris a condimentum tortor. Cras nec molestie est. Nulla vel facilisis metus. Quisque tempus fermentum enim, in feugiat sem laoreet</p>
									<div class="testimonial_user">
										<div class="testimonial_image mx-auto">
											<img src="<?php echo site_url('assets/images/staff/2.png')?>" alt="https://unsplash.com/@remdesigns">
										</div>
										<div class="testimonial_name">R. Arif Budi Setianto, MSi., SSi., Apt</div>
										<div class="testimonial_title">Dosen Fakultas Farmasi UAD</div>
									</div>
								</div>
							</div>

							<!-- Testimonials Item -->
							<div class="owl-item">
								<div class="testimonials_item text-center">
									<p class="testimonials_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae neque libero. Vivamus vel interdum massa. Mauris ut felis vel diam pretium eleifend vel eu neque. Mauris a condimentum tortor. Cras nec molestie est. Nulla vel facilisis metus. Quisque tempus fermentum enim, in feugiat sem laoreet</p>
									<div class="testimonial_user">
										<div class="testimonial_image mx-auto">
											<img src="<?php echo site_url('assets/images/staff/3.png')?>" alt="https://unsplash.com/@remdesigns">
										</div>
										<div class="testimonial_name">Dr. Iis Wahyuningsih, MSi, Apt </div>
										<div class="testimonial_title">Dosen Fakultas Farmasi UAD</div>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- Cities -->
	<!-- Call to Action -->

	<div class="cta_1">
		<div class="cta_1_background" style="background-image:url(images/cta_1.jpg)"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					
					<div class="cta_1_content d-flex flex-lg-row flex-column align-items-center justify-content-start">
						<h3 class="cta_1_text text-lg-left text-center">Ingin informasi lebih lanjut.? <span>Hubungi Admin kami</span></h3>
						<div class="cta_1_phone">Telpon :   +6289680988232</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				
				<!-- Footer About -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">
						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src="images/logo.png" alt="">
									<span>sisela farmasi uad</span>
								</div>
							</a>
						</div>
					</div>
					<div class="footer_social">
						<ul class="footer_social_list">
							<li class="footer_social_item"><a href="#"><i class="fab fa-pinterest"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-dribbble"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-behance"></i></a></li>
						</ul>
					</div>
					<div class="footer_about">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, viverra sit amet lacus. Nam gravida  quis ferme semper augue.</p>
					</div>
				</div>
				
				<!-- Footer Useful Links -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">useful links</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="#">Universitas Ahmad Dahlan</a></li>
						<li class="useful_links_item"><a href="#">Fakultas Farmasi UAD</a></li>
						<li class="useful_links_item"><a href="#">PMD UAD</a></li>
						<li class="useful_links_item"><a href="#">PORTAL UAD</a></li>
						<li class="useful_links_item"><a href="#">BIMAWA UAD</a></li>
						<li class="useful_links_item"><a href="#"></a></li>
					</ul>
				</div>

				<!-- Footer Contact Form -->
				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">say hello</div>
					<div class="footer_contact_form_container">
						<form id="footer_contact_form" class="footer_contact_form" action="post">
							<input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required.">
							<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
							<textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
							<button id="contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">send</button>
						</form>
					</div>
				</div>

				<!-- Footer Contact Info -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">contact info</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div></div>
							<div class="contact_info_text">Kampus 3
							Alamat: Jl. Prof. DR. Soepomo Sh, Warungboto, Kec. Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55164</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div></div>
							<div class="contact_info_text">(0274) 563515</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/message.svg" alt=""></div></div>
							<div class="contact_info_text"><a href="mailto:farmasi@uad.ac.id?Subject=Hello" target="_top">farmasi@uad.ac.id</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>
    <div class="credits">
		<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Fakultas Farmasi Universitas Ahmad Dahlan
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
	</div>
<div class="modal" id="modalmasuk">
  	<div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Masuk Akun</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
	  <form action="<?php echo site_url('mahasiswa/login/masuk_mahasiswa')?>" method="POST">
		<div class="form-group">
			<label for="usr">Email:</label>
			<input type="email" class="form-control" id="usr" name="xemail">
		</div>
		<div class="form-group">
			<label for="pwd">Password:</label>
			<input type="password" class="form-control" id="pwd" name="xpassword">
		</div>
		<a href="" data-toggle="modal" data-target="#lupapassword" data-dismiss="modal">Lupa password</a>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
		<a href="<?php echo site_url('mahasiswa/login')?>" class="btn btn-info">Daftar Akun</a>
		<button type="submit" class="btn btn-primary">Masuk</button>
      </div>
	  </form>

    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal" id="lupapassword">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Reset Password</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
	  </div>
	  <form action="<?php echo site_url('mahasiswa/login/lupa_password')?>" method="POST">
      <!-- Modal body -->
		<div class="modal-body">
				<div class="form-group">
					<label for="usr">Masukkan Email Anda:</label>
					<input type="email" class="form-control" id="usr" name="xemail">
				</div>
		</div>

		<!-- Modal footer -->
		<div class="modal-footer">
			<button type="submit" class="btn btn-primary">Reset</button>
		</div>
	  </form>

    </div>
  </div>
</div>
