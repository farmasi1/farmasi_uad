<script type="text/javascript" src="<?php echo site_url('assets/kalender/scripts/components/jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/kalender/scripts/dateTimePicker.min.js') ?>"></script>
<script src="<?php echo site_url('js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo site_url('styles/bootstrap4/popper.js')?>"></script>
<script src="<?php echo site_url('styles/bootstrap4/bootstrap.min.js')?>"></script>
<script src="<?php echo site_url('plugins/greensock/TweenMax.min.js')?>"></script>
<script src="<?php echo site_url('plugins/greensock/TimelineMax.min.js')?>"></script>
<script src="<?php echo site_url('plugins/scrollmagic/ScrollMagic.min.js')?>"></script>
<script src="<?php echo site_url('plugins/greensock/animation.gsap.min.js')?>"></script>
<script src="<?php echo site_url('plugins/greensock/ScrollToPlugin.min.js')?>"></script>
<script src="<?php echo site_url('plugins/OwlCarousel2-2.2.1/owl.carousel.js')?>"></script>
<script src="<?php echo site_url('plugins/scrollTo/jquery.scrollTo.min.js')?>"></script>
<script src="<?php echo site_url('plugins/easing/easing.js')?>"></script>
<script src="<?php echo site_url('js/custom.js')?>"></script>
<script src="<?php echo site_url('js/listings_custom.js')?>"></script>

