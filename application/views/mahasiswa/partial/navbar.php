<?php if($this->session->userdata('mahasiswa_nama')==null){?>
<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						<!-- Logo -->

						<div class="logo_container">
							<a href="<?php echo site_url('mahasiswa/home')?>">
								<div class="logo">
									<img src="<?php echo site_url('assets/images/uadlogo.png') ?>" alt="" width="50px">
									<span>sisela farmasi uad</span>
								</div>
							</a>
						</div>
						<nav class="main_nav">
								<ul class="main_nav_list">
								<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/home')?>">Beranda</a></li>
								<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/Barang')?>">Alat Praktikum</a></li>
								<li class="main_nav_item"><a href="" data-toggle="modal" data-target="#modalmasuk">Login</a></li>
							</ul>
						</nav>
						<!-- Phone Home -->
						<!-- Hamburger -->
						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Menu -->
		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/home')?>">Beranda</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/Barang')?>">Alat Praktikum</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="" data-toggle="modal" data-target="#modalmasuk">Login</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</header>
	<?php }else{?>
		<header class="header trans_300">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_container d-flex flex-row align-items-center trans_300">
							
							<!-- Logo -->
							
							<div class="logo_container">
								<a href="<?php echo site_url('mahasiswa/home')?>">
								<div class="logo">
									<img src="<?php echo site_url('assets/images/uadlogo.png') ?>" alt="" width="50px">
									<span>sisela farmasi uad</span>
								</div>
							</a>
						</div>
						<nav class="main_nav">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/home')?>">Beranda</a></li>
								<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/Barang')?>">Alat Praktikum</a></li>
								<?php if($this->session->userdata('mahasiswa_jenis')==1){?>
									<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/ruangan')?>">Ruangan</a></li>
								<?php }?>
								<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/riwayat')?>">Riwayat</a></li>
								<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/profile')?>">Profile</a></li>
								<li class="main_nav_item"><a href="<?php echo site_url('mahasiswa/login/keluar')?>">Logout</a></li>
							</ul>
						</nav>
						
						<!-- Phone Home -->
						
						
						<!-- Hamburger -->

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- Menu -->
		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/home')?>">Beranda</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/Barang')?>">Alat Praktikum</a>
							</div>
						</div>
					</div>
				</li>
				<?php if($this->session->userdata('mahasiswa_jenis')==1){?>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/ruangan')?>">Ruangan</a>
							</div>
						</div>
					</div>
				</li>
				<?php }?>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/riwayat')?>">Riwayat</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/profile')?>">Profile</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="<?php echo site_url('mahasiswa/login/keluar')?>">Logout</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</header>

<?php } ?>		
