<!DOCTYPE html>
<html lang="en">
<?php 
	  $this->load->view("mahasiswa/partial/head.php");
	  $pesan = $this->session->flashdata('pesan');
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_styles.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_responsive.css')?>">
<style>
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

}

div.container {
  padding: 10px;
}
</style>

<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
        <div class="home_background" style="background-image:url(<?php echo site_url('images/home/fhome2.png')?>)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						
						<!-- Main Navigation -->

                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
	</header>

	<!-- Listings -->
	<div class="listings">
		<div class="container">
			<div class="row">
				<!-- Listings -->
				<div class="col-lg-12">
					<!-- ini buat pesan flash -->
					<?php if($pesan){ ?>
					<div class="container">
						<div class="alert alert-info alert-dismissible fade show">
                        	<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong><?php echo $pesan?></strong>
                    <!-- listing item bs -->
                        </div>
					</div>
					<?php }?>
					<!-- ini buat pesan flash -->
                    <div class="row polaroid" style="padding:10px;">
                        <div class="col-6">
                        <?php foreach($mahasiswa as $data): ?>
                            <div class="card" style="width:400px">
                                <img class="card-img-top" src="<?php echo site_url("assets/images/profile/$data->mahasiswa_foto")?>" alt="Card image" style="width:100%">
                                <div class="card-body">
                                    <h4 class="card-title"><?php echo $data->mahasiswa_nama?></h4>
									<div class="row">
										<div class="col-4">
											Nim
										</div>
										<div class="col-8">
											<?php echo $data->mahasiswa_nim?>
										</div>
									</div>
									<div class="row">
										<div class="col-4">
											Alamat
										</div>
										<div class="col-8">
											<?php echo $data->mahasiswa_alamat?>
										</div>
									</div>
									<div class="row">
										<div class="col-4">
											No Hp
										</div>
										<div class="col-8">
											<?php echo $data->mahasiswa_nohp?>
										</div>
									</div>
									<div class="row">
										<div class="col-4">
											Jenis Kelamin
										</div>
										<div class="col-8">
											<?php echo $data->mahasiswa_jk?>
										</div>
									</div>
									<div class="row">
										<div class="col-4">
											Alamat E-Mail
										</div>
										<div class="col-8">
											<?php echo $data->mahasiswa_email?>
										</div>
									</div>
									<div class="row">
										<div class="col-4">
											Instansi
										</div>
										<div class="col-8">
											<?php echo $data->mahasiswa_instansi?>
										</div>
									</div>
									<hr>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal<?php echo $data->mahasiswa_id?>">
										Edit Profile
									</button>
                                </div>
                            </div>
                        <?php endforeach?>
                        </div>
						<?php if($data->mahasiswa_jenis == 1){?>
						<?php if($verifikasi == 0){?>
                        <div class="col-6">
							<div class="container">
								<div class="alert alert-success">
									<strong>Lengkapi Data Anda</strong>
								</div>
								<div class="alert alert-info">
									Download Formulir Persyaratan <strong><a href="https://ffarmasi.uad.ac.id/unduhan/#tab-id-2" target="_blank">DI SINI</a></strong>
								</div>
								<div class="card">
									<div class="card-header">Silahkan masukkan file dalam Format PDF</div>
									<div class="card-body" style="background-color:white">
										<form action="<?php echo site_url('mahasiswa/profile/verifikasi') ?>" method="POST" enctype="multipart/form-data">
											<div class="form-group" hidden>
												<strong><label for="usr" style="color:black;">Skripsi 2 / Surat Terlibat Penelitian Dosen</label></strong>
												<input type="text" class="form-control" id="usr" name="xid" value="<?php echo $this->session->userdata('mahasiswa_id')?>">
											</div>
											<div class="form-group">
												<strong><label for="usr" style="color:black;">Skripsi 2 / Surat Terlibat Penelitian Dosen</label></strong>
												<input type="file" class="form-control" id="usr" name="xskripsi" required>
											</div>
											<div class="form-group">
												<strong><label for="usr" style="color:black;">Bukti Transfer</label></strong>
												<input type="file" class="form-control" id="usr" name="xbukti" required>
											</div>
									</div> 
									<div class="card-footer"><button type="submit" class="btn btn-primary">Kirim</button></div>
										</form>
								</div>
							</div>
                        </div>
						<?php }elseif($verifikasi == 1){?>
						<div class="col-6">
							<div class="container">
								<div class="alert alert-success">
									<strong>Sedang Proses Verifikasi</strong>
								</div>
							</div>
                        </div>
						<?php }elseif($verifikasi == 2){?>
						<div class="col-6">
							<div class="container">
								<div class="alert alert-success">
									<strong>Sudah Terverifikasi</strong>
								</div>
							</div>
                        </div>
						<?php }?>
						<?php }else{?>
						<?php if($verifikasi == 0){?>
						<div class="col-6">
							<div class="container">
								<div class="alert alert-success">
									<strong>Lengkapi Data Anda</strong>
								</div>
								<div class="alert alert-info">
									Download Formulir Persyaratan <strong><a href="https://ffarmasi.uad.ac.id/unduhan/#tab-id-2" target="_blank">DI SINI</a></strong>
								</div>
								<div class="card">
									<div class="card-header">Silahkan masukkan file dalam Format PDF</div>
									<div class="card-body" style="background-color:white">
										<form action="<?php echo site_url('mahasiswa/profile/verifikasi') ?>" method="POST" enctype="multipart/form-data">
											<div class="form-group" hidden>
												<strong><label for="usr" style="color:black;">Skripsi 2 / Surat Terlibat Penelitian Dosen</label></strong>
												<input type="text" class="form-control" id="usr" name="xid" value="<?php echo $this->session->userdata('mahasiswa_id')?>">
											</div>
											<div class="form-group">
												<strong><label for="usr" style="color:black;">Surat Izin Pemakaian Lab</label></strong>
												<input type="file" class="form-control" id="usr" name="xizin" required>
											</div>
											<div class="form-group">
												<strong><label for="usr" style="color:black;">Bukti Transfer</label></strong>
												<input type="file" class="form-control" id="usr" name="xbukti" required>
											</div>
											<div class="form-group">
												<strong><label for="usr" style="color:black;">Upload KTP/SIM/PASSPORT</label></strong>
												<input type="file" class="form-control" id="usr" name="xpassport" required>
											</div>
									</div> 
									<div class="card-footer"><button type="submit" class="btn btn-primary">Kirim</button></div>
										</form>
								</div>
							</div>
                        </div>
						<?php }elseif($verifikasi == 1){?>
						<div class="col-6">
							<div class="container">
								<div class="alert alert-success">
									<strong>Sedang Proses Verifikasi</strong>
								</div>
							</div>
                        </div>
						<?php }elseif($verifikasi == 2){?>
						<div class="col-6">
							<div class="container">
								<div class="alert alert-success">
									<strong>Sudah Terverifikasi</strong>
								</div>
							</div>
                        </div>
						<?php }?>
						<?php }?>
						
                    </div>
                        <!-- listing item bs -->
				</div>

			</div>
		</div>
	</div>

	<!-- Newsletter -->

		<!-- ini buat footer -->
        <?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->

</div>

<!-- ini buat js -->
<?php 
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->
</body>
</html>

<?php foreach($mahasiswa as $data): ?>
<div class="modal" id="myModal<?php echo $data->mahasiswa_id?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Profile</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
	  <form action="<?php echo site_url('mahasiswa/profile/update_profile')?>" method="POST" enctype="multipart/form-data">
                            <div class="container">
                                <div class="row">
									<div class="col-6" hidden>
                                        <div class="form-group">
                                            <label for="usr" style="color:black">id</label>
                                            <input type="text" class="form-control" id="usr" name="xid" value="<?php echo $data->mahasiswa_id?>" required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Nama Lengkap</label>
                                            <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $data->mahasiswa_nama?>" required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">NIM / NIP </label>
                                            <input type="text" class="form-control" id="usr" name="xnim" value="<?php echo $data->mahasiswa_nim?>"required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Alamat</label>
                                            <input type="text" class="form-control" id="usr" name="xalamat" value="<?php echo $data->mahasiswa_alamat?>"required>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Nama Instansi</label>
                                            <input type="text" class="form-control" id="usr" name="xinstansi" value="<?php echo $data->mahasiswa_instansi?>"required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">No KTP</label>
                                            <input type="text" class="form-control" id="usr" name="xktp" value="<?php echo $data->mahasiswa_ktp?>"required>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">No Handphone</label>
                                            <input type="text" class="form-control" id="usr" name="xnohp" value="<?php echo $data->mahasiswa_nohp?>"required>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="usr" style="color:black">Masukkan Password</label>
                                            <input type="password" class="form-control" id="usr" name="xpassword" value="<?php echo $data->mahasiswa_alamat?>"required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                    </div>
                                </div>
                            </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
		<button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
                        </form>

    </div>
  </div>
</div>
<?php endforeach?>
