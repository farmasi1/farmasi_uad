<!DOCTYPE html>
<html lang="en">
<?php 
    $this->load->view('admin1/partials/head.php');
    $ci =& get_instance();
    $ci->load->model('m_admin_barang');
    $ci->load->model('m_mhs_barang');
    $ci->load->model('m_transaksi');
?>
<body>

<div class="container">
    <div class="row">
        <div class="col-12" style="text-align: center;">
            <h3>Riwayat Penyewaan Alat Praktikum <br> Fakultas Farmasi <br> Universtias Ahmad Dahlan</h3>
        </div>
    </div>
    <hr>
    <?php foreach($biodata as $data):?>
    <div class="row">
        <div class="col-4">
            Nama Mahaiswa
        </div>
        <div class="col-8">
            <?php echo $data->mahasiswa_nama?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            NIM/NIP
        </div>
        <div class="col-8">
            <?php echo $data->mahasiswa_nim?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Alamat
        </div>
        <div class="col-8">
            <?php echo $data->mahasiswa_alamat?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Instansi
        </div>
        <div class="col-8">
            <?php echo $data->mahasiswa_instansi?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Nomer Handphone
        </div>
        <div class="col-8">
            <?php echo $data->mahasiswa_nohp?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            Email
        </div>
        <div class="col-8">
            <?php echo $data->mahasiswa_email?>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            No KTP
        </div>
        <div class="col-8">
            <?php echo $data->mahasiswa_ktp?>
        </div>
    </div>
    <?php endforeach?>
    <hr>
    <div class="container">
        <p>Riwayat Peminjaman Barang Per Jam</p>            
        <table class="table">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Harga Sewa</th>
                <th>Tanggal Sewa</th>
                <th>Jam Penyewaan</th>
                <th>Total Harga</th>
            </tr>
            </thead>
            <tbody>
                <!-- ini untuk riwayat perjam -->
            <?php $no=0; foreach($riwayat_jam as $data): $no++; ?>
            <tr>
                <td><?php echo $no?></td>
                <td><?php echo $data->barang_nama?></td>
                <td><?php echo $data->barang_harga?></td>
                <td><?php echo $data->tgl_sewa?></td>
                <td>
                <?php
                    $total_jam=0;
                    $durasi_jam = $ci->m_mhs_barang->ambil_jam($data->kode_penyewaan);
                    foreach($durasi_jam as $datajam){
                        $durasi = explode(",",$datajam->durasi_jam);
                        foreach($durasi as $key){
                            echo $ci->m_transaksi->ambil_jam($key)."<br>";
                            $total_jam++;
                            }
                    }
                ?>
                </td>
                <td>
                    <?php echo $data->total?>
                </td>
            </tr>
            <?php endforeach?>
            <!-- ini untuk riwayat jam -->
            <!-- ini untuk riwayat perhari -->
            <?php $no=0; foreach($riwayat_hari as $data): $no++; ?>
            <tr>
                <td><?php echo $no?></td>
                <td><?php echo $data->barang_nama?></td>
                <td><?php echo $data->barang_harga?></td>
                <td>
                <?php      
                    $tanggal_sewa = $ci->m_mhs_barang->ambil_tanggal_riwayat($data->kode_penyewaan);
                    $a = array();
                    foreach($tanggal_sewa as $v){
                        $loop = explode(",",$v->tgl_sewa);
                        foreach ($loop as $key => $value) {
                            $a[] = $value;
                        }
                    }
                    $total = count($a);
                    for($i =0; $i<$total; $i++){
                        echo $a[$i]."<br>";
                    }
                    ?>
                </td>
                <td>-</td>
                <td>
                    <?php echo $data->total?>
                </td>
            </tr>
            <?php endforeach?>
            <!-- ini untuk riwayat perhari -->
            </tbody>
        </table>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-10">
                Total Biaya :
            </div>
            <div class="col-2">
                <?php echo $total_biaya?>
            </div>
        </div>
    </div>
</div>

</body>
</html>