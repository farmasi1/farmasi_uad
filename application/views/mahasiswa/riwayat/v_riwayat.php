<!DOCTYPE html>
<html lang="en">
<?php 
      $this->load->view("mahasiswa/partial/head.php");
?>
	<?php 
	    $ci =& get_instance();
		$ci->load->model('m_transaksi');
	?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_styles.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_responsive.css')?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

}

div.container {
  padding: 10px;
}
</style>

<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
        <div class="home_background" style="background-image:url(<?php echo site_url('images/farmasi3.jpg')?>)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						
						<!-- Main Navigation -->

                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
	</header>

	<!-- Listings -->

	<div class="listings">
		<div class="container">
			<div class="row">
				<!-- Listings -->
				<div class="col-lg-12">
                    <!-- listing item bs -->
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card polaroid">
								<div class="container">
								<center>
									<h3>Riwayat Peminjaman Alat</h3>
								</center>
									<hr>
									<table class="table">
										<thead class="thead-dark">
											<tr>
												<th>No</th>
												<th>Nama Barang</th>
												<th>Tanggal Sewa</th>
												<th>Total Harga</th>
												<th>Kode Penyewaan</th>
												<th>Sampel</th>
												<th>Tujuan Penyewaan</th>
												<th>Status</th>
												<th>Option</th>
											</tr>
										</thead>
										<tbody>
											<?php $no = 0; foreach($riwayat as $data): $no++;?>
											<?php $tanggal = substr($data->tgl_sewa, 0, 10);?>
											<tr>
												<td><?php echo $no?></td>
												<td><?php echo $data->barang_nama?></td>
												<td><?php echo $tanggal?></td>
												<td><?php echo $data->total?></td>
												<td>
													<?php echo $data->kode_penyewaan?>
												</td>
												<td>
													<?php echo $data->sampel?>
												</td>
												<td>
													<?php echo $data->tujuan_penyewaan?>
												</td>
												<td>
													<?php if($data->status_transaksi==1){
														echo "Menunggu Konfirmasi";
													}elseif($data->status_transaksi==2){
														echo "Diterima";
													}elseif($data->status_transaksi==3){
														echo "Ditolak";
													}elseif($data->status_transaksi==4){
														echo "Batal";
													}?>
												</td>
												<td>
													<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal<?php echo $data->kode_penyewaan?>">
														Detail
													</button>
												</td>
											</tr>
											<?php endforeach?>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td><a href="<?php echo site_url("mahasiswa/riwayat/cetak_riwayat/$nim")?>" class="btn btn-success" role="button">Cetak Riwayat</a></td>
												
											</tr>
										</tbody>
									</table>
								</div>
								<div class="container">
								<center>
									<h3>Riwayat Peminjaman Ruangan</h3>
								</center>
									<hr>
									<table class="table">
										<thead class="thead-dark">
											<tr>
												<th>No</th>
												<th>Nama Ruangan</th>
												<th>Tanggal Sewa</th>
												<th>Jam Penyewaan</th>
												<th>Tujuan Penyewaan</th>
												<th>Status</th>
												<th>Kode Penyewaan</th>
											</tr>
										</thead>
										<tbody>
											<?php $no = 0; foreach($riwayat_ruangan as $data): $no++; $durasi_ruangan = explode(",", $data->durasi_jam)?>
											<?php $tanggal = substr($data->tgl_sewa, 0, 10);?>
											<tr>
												<td><?php echo $no?></td>
												<td><?php echo $data->ruang_nama?></td>
												<td><?php echo $tanggal?></td>
												<td>
												<?php foreach($durasi_ruangan as $key):?>
													<?php echo $ci->m_transaksi->ambil_jam($key)."<br>"?>
												<?php endforeach?>
												</td>
												<td>
													<?php echo $data->alasan_peminjaman?>
												</td>
												<td>
													<?php if($data->status_transaksi==1){
														echo "Menunggu Konfirmasi";
													}elseif($data->status_transaksi==2){
														echo "Diterima";
													}elseif($data->status_transaksi==3){
														echo "Ditolak";
													}elseif($data->status_transaksi==4){
														echo "Batal";
													}?>
												</td>
												<td>
													<?php echo $data->kode_peminjaman_ruangan?>
												</td>
											</tr>
											<?php endforeach?>
										</tbody>
									</table>
								</div>
                        </div>                                   
                    </div>

                </div>

                    <!-- listing item bs -->

				</div>

			</div>

			<!-- <div class="row">
				<div class="col clearfix">
					<div class="listings_nav">
						<ul>
							<li class="listings_nav_item active"><a href="#">01.</a></li>
							<li class="listings_nav_item"><a href="#">02.</a></li>
							<li class="listings_nav_item"><a href="#">03.</a></li>
						</ul>
					</div>
				</div>
			</div> -->
		</div>
	</div>

	<!-- Newsletter -->

		<!-- ini buat footer -->
        <?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->

</div>

<!-- ini buat js -->
<?php 
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->

</body>

</html>
<?php foreach($riwayat as $data): $durasi = explode(",", $data->durasi_jam)?>
<div class="modal" id="myModal<?php echo $data->kode_penyewaan?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Detail Penyewaan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

	  <!-- Modal body -->
	  <div class="modal-body">
            <div class="form-group">
                <label for="usr">kode penyewaan:</label>
                <input type="text" class="form-control" name="xid" value="<?php echo $data->kode_penyewaan?>" readonly required>
            </div>
            <div class="form-group">
                <label for="usr">Nama Barang:</label><br>
                <span><?php echo $data->barang_nama?></span>
            </div>
			<hr>
			<div class="form-group">
				<label for="usr">Tanggal Sewa:</label><br>
				<?php 
					$tanggal = substr($data->tgl_sewa, 0, 10);
					$tanggalsemua = explode(",", $data->tgl_sewa);
				?>  
				<span>
					<?php
						foreach($tanggalsemua as $value){
							echo $value."<br>";
						}
					?>
				</span>
            </div>
            <hr>
            <?php if($data->tipe_peminjaman==2){?>
            <div class="form-group">
                <label for="usr">Lama penyewaan:</label><br>
                <span><?php echo $data->lama_sewa." Hari"?></span>
            </div>
            <?php }elseif($data->tipe_peminjaman==1){?>
				<label for="usr">Waktu Penyewaan:</label><br>
                <span>
                  <?php $jam = 0; foreach($durasi as $key): $jam++;?>
                      <?php echo "Jam ke ".$key." : ".$ci->m_transaksi->ambil_jam($key)."<br>"?>
                  <?php endforeach?>
                </span>
              <label for="usr">Total Jam : <?php echo $jam;?></label><br>
            <?php }?>
            <hr>
            <div class="form-group">
                <label for="usr">Tujuan Penyewaan:</label><br>
                <span><?php echo $data->tujuan_penyewaan?></span>
            </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<?php if($tanggal > date('Y-m-d')){?>
			<a href="<?php echo site_url("mahasiswa/transaksi/batalkan_transaksi/$data->kode_penyewaan")?>" class="btn btn-warning" role="button">Batalkan Penyewaan</a>
		<?php }else{?>
			<a href="<?php echo site_url("mahasiswa/transaksi/batalkan_transaksi/$data->kode_penyewaan")?>" class="btn btn-warning disabled" role="button">Batalkan Penyewaan</a>
		<?php }?>
      </div>

    </div>
  </div>
</div>
<?php endforeach?>
