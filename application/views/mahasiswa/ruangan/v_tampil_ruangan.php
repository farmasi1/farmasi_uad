<!DOCTYPE html>
<html lang="en">
<?php 
      $this->load->view("mahasiswa/partial/head.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_styles.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_responsive.css')?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

}

div.container {
  padding: 10px;
}
</style>

<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
        <div class="home_background" style="background-image:url(<?php echo site_url('images/home/metingroom.png')?>)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						
						<!-- Main Navigation -->

                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
	</header>

	<!-- Listings -->

	<div class="listings">
		<div class="container">
			<div class="row">
				<!-- Listings -->
				<div class="col-lg-12">
                    <!-- listing item bs -->
                    <center>
                        <div class="alert alert-info">
                            Penyewaan Ruangan
                        </div>
                    </center>
                    <hr>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card polaroid">
                                    <div class="card-header bg-primary text-white">
                                        Pilih Ruangan
                                    </div>
                                    <div class="card-body bg-light">
                                    <form action="<?php echo site_url('mahasiswa/ruangan/cek_jadwal_peminjaman_ruangan')?>" method="POST">
                                        <div class="form-group">
                                            <label for="sel1">Nama Ruangan</label>
                                                <select class="form-control" id="sel1" name="xidruang">
                                                    <?php foreach($ruangan as $data):?>
                                                    <option value="<?php echo $data->ruang_id?>"><?php echo $data->ruang_nama?></option>
                                                    <?php endforeach?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="usr">Tanggal</label>
                                            <input type="date" class="form-control" id="usr" name="xtanggal" onchange="this.form.submit();">
                                        </div>
                                    </form>

                                        <div class="form-group">
                                            <label for="email">Jam Penyewaan</label>
                                            <div class="row">
                                                <?php foreach($jam as $data):?>
                                                    <div class="col-12">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" class="form-check-input" name="xdurasi[]" value="<?php echo $data->jam_sesi?>"><span style="padding-left:20px;"><?php echo $data->jam_waktu?></span>
                                                        </label>
                                                    </div>
                                                    <?php endforeach ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                        </div>
                    </div>

                    <!-- listing item bs -->

				</div>

			</div>

			<!-- <div class="row">
				<div class="col clearfix">
					<div class="listings_nav">
						<ul>
							<li class="listings_nav_item active"><a href="#">01.</a></li>
							<li class="listings_nav_item"><a href="#">02.</a></li>
							<li class="listings_nav_item"><a href="#">03.</a></li>
						</ul>
					</div>
				</div>
			</div> -->
		</div>
	</div>

	<!-- Newsletter -->

		<!-- ini buat footer -->
        <?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->

</div>

<!-- ini buat js -->
<?php 
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->

</body>
<script>
var max_limit = 3; // Max Limit
$(document).ready(function (){
    $(".jam:input:checkbox").each(function (index){
        this.checked = (".jam:input:checkbox" < max_limit);
    }).change(function (){
        if ($(".jam:input:checkbox:checked").length > max_limit){
            this.checked = false;
        }
    });
});
</script>
</html>
