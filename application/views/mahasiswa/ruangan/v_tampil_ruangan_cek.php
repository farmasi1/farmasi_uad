<!DOCTYPE html>
<html lang="en">
<?php 
      $this->load->view("mahasiswa/partial/head.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_styles.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('styles/listings_responsive.css')?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

}

div.container {
  padding: 10px;
}
</style>

<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
        <div class="home_background" style="background-image:url(<?php echo site_url('images/farmasi3.jpg')?>)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						
						<!-- Main Navigation -->

                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
	</header>

	<!-- Listings -->

	<div class="listings">
		<div class="container">
			<div class="row">
				<!-- Listings -->
				<div class="col-lg-12">
                    <!-- listing item bs -->
                    <center>
                        <div class="alert alert-info">
                            Penyewaan Ruangan
                        </div>
                    </center>
                    <hr>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card polaroid">
                                    <div class="card-header bg-primary text-white">
                                        Pilih Ruangan
                                    </div>
                                    <div class="card-body bg-light">
                                    <form action="<?php echo site_url('mahasiswa/ruangan/cek_jadwal_peminjaman_ruangan')?>" method="POST">
                                        <div class="form-group">
                                            <label for="sel1">Nama Ruangan</label>
                                                <select class="form-control" id="sel1" name="xidruang" onchange="this.form.submit();">
                                                    <option value="<?php echo $ruang_id?>"><?php echo $ruang_nama?></option>
                                                    <?php foreach($ruangan as $data):?>
                                                    <option value="<?php echo $data->ruang_id?>"><?php echo $data->ruang_nama?></option>
                                                    <?php endforeach?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="usr">Tanggal</label>
                                            <input type="date" class="form-control" id="usr" name="xtanggal" value="<?php echo $tanggal?>" onchange="this.form.submit();">
                                        </div>
                                    </form>
                                    <form action="<?php echo site_url('mahasiswa/transaksi/simpan_peminjaman_ruangan')?>" method="POST">
                                        <div class="form-group" hidden>
                                            <label for="sel1">Nama Ruangan</label>
                                                <select class="form-control" id="sel1" name="xidruang">
                                                    <option value="<?php echo $ruang_id?>"><?php echo $ruang_nama?></option>
                                                </select>
                                        </div>
                                        <div class="form-group" hidden>
                                            <label for="email">Id Peminjamn:</label>
                                            <input type="number" class="form-control col-12" name="xnim" value="<?php echo $this->session->userdata('mahasiswa_nim')?>" readonly>
                                        </div>
                                        <div class="form-group" hidden>
                                            <label for="usr">Tanggal</label>
                                            <input type="date" class="form-control" id="usr" name="xtanggal" value="<?php echo $tanggal?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Jam Penyewaan</label>
                                            <div class="row">
                                                <?php foreach($jam as $data):?>
                                                    <div class="col-12">
                                                        <label class="form-check-label">
                                                        <?php if(in_array($data->jam_sesi,$jam_terpakai)){?>
                                                            <input type="checkbox" class="form-check-input" name="xdurasi[]" value="<?php echo $data->jam_sesi?>" disabled ><span class="badge badge-danger" style="padding-left:35px;"><?php echo $data->jam_waktu?></span>
                                                        <?php }else{?>
                                                            <input type="checkbox" class="jam" name="xdurasi[]" value="<?php echo $data->jam_sesi?>"><span style="padding-left:20px;"><?php echo $data->jam_waktu?></span>
                                                        <?php }?>
                                                        </label>
                                                    </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Alasan Peminjaman</label>
                                                <div class="form-check">
                                                    <label class="form-check-label" for="radio1">
                                                <input type="radio" class="form-check-input" id="radio1" name="xtujuan" value="sidang" checked>Sidang
                                            </label>
                                        </div>
                                            <div class="form-check">
                                            <label class="form-check-label" for="radio2">
                                                <input type="radio" class="form-check-input" id="radio2" name="xtujuan" value="rapat">Rapat
                                            </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary col-12">Kirim</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </form>
                                </div>
                                    
                        </div>

                    </div>

                    <!-- listing item bs -->

				</div>

			</div>

			<!-- <div class="row">
				<div class="col clearfix">
					<div class="listings_nav">
						<ul>
							<li class="listings_nav_item active"><a href="#">01.</a></li>
							<li class="listings_nav_item"><a href="#">02.</a></li>
							<li class="listings_nav_item"><a href="#">03.</a></li>
						</ul>
					</div>
				</div>
			</div> -->
		</div>
	</div>

	<!-- Newsletter -->

		<!-- ini buat footer -->
        <?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->

</div>

<!-- ini buat js -->
<?php 
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->

</body>

<script>
var max_limit = 3; // Max Limit
$(document).ready(function (){
    $(".jam:input:checkbox").each(function (index){
        this.checked = (".jam:input:checkbox" < max_limit);
    }).change(function (){
        if ($(".jam:input:checkbox:checked").length > max_limit){
            this.checked = false;
        }
    });
});
</script>
</html>
