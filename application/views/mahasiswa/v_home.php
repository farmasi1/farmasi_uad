<!DOCTYPE html>
<html lang="en">
<?php 
	  $this->load->view("mahasiswa/partial/head.php");
	  $flash_msg = $this->session->flashdata('flash_msg');
?>
<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">		
		<!-- Home Slider -->
		<div class="home_slider_container">
			<div class="owl-carousel owl-theme home_slider">
				<!-- Home Slider Item -->
				<div class="owl-item home_slider_item">
					<!-- Image by https://unsplash.com/@aahubs -->
					<div class="home_slider_background" style="background-image:url(<?php echo site_url('images/home/fhome2.png')?>)"></div>
					<div class="home_slider_content_container text-center">
						<div class="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">LABORATORIUM</h1>
						</div>
					</div>
				</div>

				<!-- Home Slider Item -->
				<div class="owl-item home_slider_item">
					<!-- Image by https://unsplash.com/@aahubs -->
					<div class="home_slider_background" style="background-image:url(<?php echo site_url('images/home/fhome3.png')?>)"></div>
					<div class="home_slider_content_container text-center">
						<div class="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Alat LAb</h1>
						</div>
					</div>
				</div>

				<!-- Home Slider Item -->
				<div class="owl-item home_slider_item">
					<!-- Image by https://unsplash.com/@aahubs -->
					<div class="home_slider_background" style="background-image:url(<?php echo site_url('images/home/fhome1.png')?>)"></div>
					<div class="home_slider_content_container text-center">
						<div class="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Peminjaman Alat</h1>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Home Slider Nav -->
			<!-- <div class="home_slider_nav_left home_slider_nav d-flex flex-row align-items-center justify-content-end">
				<img src="images/nav_left.png" alt="">
			</div> -->

		</div>

	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">
						<!-- Logo -->
						<!-- Main Navigation -->
                        <?php 
                            $this->load->view("mahasiswa/partial/navbar.php");
                        ?>		
						<!-- Phone Home -->

						
					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->


	</header>
	
	<!-- Search Box -->

	<div class="search_box">
		<div class="container">
			<div class="row">
				<div class="col">

					<div class="search_box_outer">
						<div class="">
							<!-- Search Box Title -->
							<!-- <div class="search_box_title text-center">
								<div class="search_box_title_inner">
									<div class="search_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src="<?php echo site_url('images/search.png')?>" alt=""></div>
									<span>search your home</span>
								</div>
							</div> -->
							<!-- Search Form -->
							
						</div>
					</div>

				</div>
			</div>
		</div>		
	</div>

	<!-- Featured Properties -->

	<div class="featured" id="grad1">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section_title text-center">
						<h3>Alat Laboratorium</h3>
						<!-- <span class="section_subtitle">Lihat Alat Laboratorium Terbaik Kami</span> -->
					</div>
				</div>
			</div>
			
			<form action="<?php echo site_url('mahasiswa/barang/cari_barang') ?>" method="POST">
				<div class="row" style="padding: 20px;">
					<div class="col-12">
						<div class="wrap">
						<div class="search">
								<input type="text" class="searchTerm" name="xsearch" placeholder="Cari Alat Laboratorium">
								<button type="submit" class="searchButton">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
                <div class="row">
					<?php foreach($barang as $data): ?>
                        <div class="col-3">
                            <div class="container">
                                <div class="row featured_row">
                                    <div class="featured_card_col">
                                        <div class="featured_card_container">
                                            <div class="card featured_card trans_300">
                                                <div class="featured_panel">Alat</div>
                                                <img class="card-img-top" src="<?php echo site_url('assets/images/barang/'.$data->barang_foto)?>" alt="https://unsplash.com/@breather">
                                                <div class="card-body">
                                                    <div class="card-title"><a href="<?php echo site_url("mahasiswa/barang/detail_barang/$data->barang_id") ?>"><?php echo $data->barang_nama?></a></div>
                                                    <div class="card-text"><?php echo $data->barang_keterangan?></div>
                                                </div>
                                            </div>
                                            <div class="featured_card_box d-flex flex-row align-items-center trans_300">
                                                <img src="<?php echo site_url('images/tag.svg') ?>">
                                                <div class="featured_card_box_content">
													<?php if($data->barang_tipe_peminjaman==1){?>
														<div class="featured_card_price_title">Harga Sewa / Jam</div>
													<?php }else{?>
														<div class="featured_card_price_title">Harga Sewa / Hari</div>
													<?php }?>
                                                    <div class="featured_card_price"><a href="<?php echo site_url("mahasiswa/barang/detail_barang/$data->barang_id") ?>" style="color:white">Rp <?php echo $data->barang_harga?></a></div>
											</div>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php endforeach?>	
                </div>
            </div>
			<div class="container">
				<center><div class="button news_post_button"><a href="<?php echo site_url('mahasiswa/barang')?>">Selengkapnya</a></div></center>
			</div>
		</div>

	<!-- ini buat footer -->
	<?php $this->load->view('mahasiswa/partial/footer.php')?>
	<!-- ini buat footer -->
	

</div>
<!-- ini buat js -->
<?php 
if($flash_msg){ 
	echo '<script language="javascript">';
	echo "alert('$flash_msg')";
	echo '</script>';
  }
      $this->load->view("mahasiswa/partial/js.php");
?>
<!-- ini buat js -->
<!-- <script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/scrollTo/jquery.scrollTo.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script> -->


</html>

