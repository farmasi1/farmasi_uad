-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2019 at 09:47 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_farmasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_nama` varchar(150) NOT NULL,
  `admin_email` varchar(150) NOT NULL,
  `admin_password` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_nama`, `admin_email`, `admin_password`) VALUES
(1, 'admin farmasi', 'admin@gmail.com', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `barang_id` int(11) NOT NULL,
  `barang_nama` varchar(150) NOT NULL,
  `barang_keterangan` text NOT NULL,
  `barang_status` int(11) NOT NULL,
  `barang_harga` int(11) NOT NULL,
  `barang_kondisi` varchar(150) NOT NULL,
  `barang_tipe_peminjaman` int(11) NOT NULL,
  `barang_min_jam` int(11) NOT NULL,
  `barang_max_jam` int(11) NOT NULL,
  `barang_max_hari` int(11) NOT NULL,
  `barang_foto` varchar(150) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`barang_id`, `barang_nama`, `barang_keterangan`, `barang_status`, `barang_harga`, `barang_kondisi`, `barang_tipe_peminjaman`, `barang_min_jam`, `barang_max_jam`, `barang_max_hari`, `barang_foto`, `create_at`) VALUES
(17, 'ADLC', 'oke', 0, 25000, 'Terawat', 1, 1, 4, 0, '7cc2ea5322016dcc228da1ca8c17321e.png', '2019-12-24'),
(18, 'LPPM', 'mantap', 0, 3500, 'Terawat', 1, 0, 5, 0, '8653b0266e4c04270730f004a5bb640b.png', '2019-12-24'),
(19, 'Galon', 'enak galon', 0, 25000, 'Terawat', 2, 0, 0, 6, '92774c521fd0e9c6da8194c3f772e503.jpg', '2019-12-24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jam`
--

CREATE TABLE `tbl_jam` (
  `jam_id` int(11) NOT NULL,
  `jam_waktu` char(50) NOT NULL,
  `jam_sesi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jam`
--

INSERT INTO `tbl_jam` (`jam_id`, `jam_waktu`, `jam_sesi`) VALUES
(1, '06:00 - 07:00', 1),
(2, '07:00 - 08:00', 2),
(3, '08:00 - 09:00', 3),
(4, '09:00 - 10:00', 4),
(5, '10:00 - 11:00', 5),
(6, '11:00 - 12:00', 6),
(7, '12:00 - 13:00', 7),
(8, '13:00 - 14:00', 8),
(9, '14:00 - 15:00', 9),
(10, '15:00 - 16:00', 10),
(11, '16:00 - 17:00', 11),
(12, '17:00 - 18:00', 12),
(13, '18:00 - 19:00', 13),
(14, '19:00 - 20:00', 14),
(15, '20:00 - 21:00', 15),
(16, '21:00 - 22:00', 16),
(17, '22:00 - 23:00', 17),
(18, '23:00 - 24:00', 18),
(19, '00:00 - 01:00', 19),
(20, '01:00 - 02:00', 20),
(21, '02:00 - 03:00', 21),
(22, '03:00 - 04:00', 22),
(23, '05:00 - 06:00', 23);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mahasiswa`
--

CREATE TABLE `tbl_mahasiswa` (
  `mahasiswa_id` int(11) NOT NULL,
  `mahasiswa_nim` varchar(150) NOT NULL,
  `mahasiswa_jenis` int(11) NOT NULL,
  `mahasiswa_nama` varchar(150) NOT NULL,
  `mahasiswa_alamat` text NOT NULL,
  `mahasiswa_email` varchar(150) NOT NULL,
  `create_at` date NOT NULL,
  `mahasiswa_nohp` char(50) NOT NULL,
  `mahasiswa_password` varchar(150) NOT NULL,
  `mahasiswa_ktp` varchar(150) NOT NULL,
  `mahasiswa_jk` char(20) NOT NULL,
  `mahasiswa_aktif` int(11) NOT NULL,
  `mahasiswa_foto` varchar(150) NOT NULL,
  `mahasiswa_instansi` varchar(150) NOT NULL,
  `file_bukti_transfer` varchar(150) NOT NULL,
  `file_skripsi` varchar(150) NOT NULL,
  `file_surat_izin_lab` varchar(150) NOT NULL,
  `file_ktp` varchar(150) NOT NULL,
  `mahasiswa_verifikasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mahasiswa`
--

INSERT INTO `tbl_mahasiswa` (`mahasiswa_id`, `mahasiswa_nim`, `mahasiswa_jenis`, `mahasiswa_nama`, `mahasiswa_alamat`, `mahasiswa_email`, `create_at`, `mahasiswa_nohp`, `mahasiswa_password`, `mahasiswa_ktp`, `mahasiswa_jk`, `mahasiswa_aktif`, `mahasiswa_foto`, `mahasiswa_instansi`, `file_bukti_transfer`, `file_skripsi`, `file_surat_izin_lab`, `file_ktp`, `mahasiswa_verifikasi`) VALUES
(13, '1600018095', 1, 'gema antika hariadi', '', 'gemaantikahr@gmail.com', '2019-12-21', '089680988232', '7772c4f394bc0b0d54de4361274cea7b', '123456', 'pria', 1, '', 'universitas ahmad dahlan', '2d9886b591639d97f90a099f05489f38.pdf', '65e2de91cb6f60e88ca3a96825f57c40.pdf', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ruangan`
--

CREATE TABLE `tbl_ruangan` (
  `ruang_id` int(11) NOT NULL,
  `ruang_nama` varchar(150) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ruangan`
--

INSERT INTO `tbl_ruangan` (`ruang_id`, `ruang_nama`, `create_at`) VALUES
(3, 'ruang mantap', '2019-12-20'),
(4, 'masa depan belakang', '2019-12-20'),
(5, 'ruang sidang skripsi', '2019-12-21'),
(6, 'ruang sidang praktikum', '2019-12-21'),
(7, 'ruang rapat dekan', '2019-12-21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `transaksi_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `mahasiswa_nim` varchar(150) NOT NULL,
  `total` int(11) NOT NULL,
  `tgl_sewa` varchar(150) NOT NULL,
  `lama_sewa` int(11) NOT NULL,
  `durasi_jam` varchar(150) NOT NULL,
  `tujuan_penyewaan` text NOT NULL,
  `tipe_peminjaman` int(11) NOT NULL,
  `sampel` varchar(150) NOT NULL,
  `kode_penyewaan` varchar(150) NOT NULL,
  `status_transaksi` int(11) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`transaksi_id`, `barang_id`, `mahasiswa_nim`, `total`, `tgl_sewa`, `lama_sewa`, `durasi_jam`, `tujuan_penyewaan`, `tipe_peminjaman`, `sampel`, `kode_penyewaan`, `status_transaksi`, `create_at`) VALUES
(32, 19, '1600018095', 150000, '2019-12-17,2019-12-18,2019-12-19,2019-12-20,2019-12-21,2019-12-22', 6, '', 'penelitian', 2, '', '1600018095407aad', 2, '2019-12-24'),
(33, 17, '1600018095', 100000, '2019-12-25', 0, '1,2,3,4', 'enak aja', 1, '', '1600018095c2cfb9', 4, '2019-12-24'),
(34, 17, '1600018095', 100000, '2019-12-25', 0, '1,3,6,9', 'praktikum', 1, 'okelah', '16000180954695bc', 2, '2019-12-24'),
(35, 18, '1600018095', 17500, '2019-12-25', 0, '19,20,21,22,23', 'praktikum', 1, 'cintaku padamau', '160001809567a8cd', 2, '2019-12-24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_ruangan`
--

CREATE TABLE `tbl_transaksi_ruangan` (
  `transaksi_ruang_id` int(11) NOT NULL,
  `ruang_id` int(11) NOT NULL,
  `mahasiswa_nim` varchar(150) NOT NULL,
  `tgl_sewa` date NOT NULL,
  `durasi_jam` char(50) NOT NULL,
  `alasan_peminjaman` varchar(150) NOT NULL,
  `status_transaksi` int(11) NOT NULL,
  `create_at` date NOT NULL,
  `kode_peminjaman_ruangan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`barang_id`);

--
-- Indexes for table `tbl_jam`
--
ALTER TABLE `tbl_jam`
  ADD PRIMARY KEY (`jam_id`);

--
-- Indexes for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  ADD PRIMARY KEY (`mahasiswa_id`);

--
-- Indexes for table `tbl_ruangan`
--
ALTER TABLE `tbl_ruangan`
  ADD PRIMARY KEY (`ruang_id`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`transaksi_id`);

--
-- Indexes for table `tbl_transaksi_ruangan`
--
ALTER TABLE `tbl_transaksi_ruangan`
  ADD PRIMARY KEY (`transaksi_ruang_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `barang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_jam`
--
ALTER TABLE `tbl_jam`
  MODIFY `jam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  MODIFY `mahasiswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_ruangan`
--
ALTER TABLE `tbl_ruangan`
  MODIFY `ruang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `transaksi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_transaksi_ruangan`
--
ALTER TABLE `tbl_transaksi_ruangan`
  MODIFY `transaksi_ruang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
