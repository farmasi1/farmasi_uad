-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2020 at 04:24 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_farmasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `barang_id` int(11) NOT NULL,
  `barang_nama` varchar(150) NOT NULL,
  `barang_keterangan` text NOT NULL,
  `barang_status` int(11) NOT NULL,
  `barang_harga` int(11) NOT NULL,
  `barang_kondisi` varchar(150) NOT NULL,
  `barang_tipe_peminjaman` int(11) NOT NULL,
  `barang_min_jam` int(11) NOT NULL,
  `barang_max_jam` int(11) NOT NULL,
  `barang_max_hari` int(11) NOT NULL,
  `barang_foto` varchar(150) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`barang_id`, `barang_nama`, `barang_keterangan`, `barang_status`, `barang_harga`, `barang_kondisi`, `barang_tipe_peminjaman`, `barang_min_jam`, `barang_max_jam`, `barang_max_hari`, `barang_foto`, `create_at`) VALUES
(5, 'HPLC-Det UV', 'Biaya tidak termasuk eluen', 0, 200000, 'Terawat', 2, 1, 4, 3, '4e97f69b95fa08389032e91bd22caed2.jpg', '2020-03-09'),
(6, 'HPLC Auto Injector', 'Biaya tidak termasuk eluen', 0, 250000, 'Terawat', 2, 1, 5, 3, '21c300e770d9a6263593869d05c32316.jpg', '2020-03-09'),
(7, 'GC detector FID/MS', 'Khusus Detector FID diskon 50% Biaya termasuk gas.', 0, 160000, 'Terawat', 1, 1, 6, 0, '82148b23cc6d8d22be381460c50d2917.jpg', '2020-03-09'),
(10, 'TLC Scanner 3', 'Biaya tidak termasuk eluen', 0, 40000, 'Terawat', 1, 1, 6, 0, 'd34a838a7d9b5210b480f4dc5d889cf5.png', '2020-03-09'),
(11, 'Spektrofotometer UV 1900', '-', 0, 30000, 'Terawat', 1, 1, 6, 0, 'e6959cc7eb2e5d1c3c9b08e880023c67.jpg', '2020-03-09'),
(12, 'Spektroflourometer', '-', 0, 35000, 'Terawat', 1, 1, 6, 0, '15805ba35c73c2e1d87fdf14df003cab.jpg', '2020-03-09'),
(13, 'Freeze Dryer', '-', 0, 16000, 'Terawat', 1, 1, 6, 0, '8c2c0021e56e8901bbdc858cc8cd3d24.jpg', '2020-03-09'),
(14, 'Sentrifuge', '-', 0, 20000, 'Terawat', 1, 1, 3, 0, 'a7f44ade88c295e5145ff91b35ead3dc.jpg', '2020-03-09'),
(15, 'Refrigerated centrifuge', '-', 0, 45000, 'Terawat', 1, 1, 3, 0, '2df1c3f61968bfb85fa879dffb268b1f.png', '2020-03-09'),
(16, 'PCR', 'Tidak termasuk PCR kit, dll', 0, 5000, 'Terawat', 1, 1, 3, 0, 'b84de8e4f217f4a82599c70c3851ebba.jpg', '2020-03-09'),
(17, 'Mesin tablet', 'Biaya operator Rp. 10.000 per formula', 0, 30000, 'Terawat', 1, 1, 6, 0, 'da31073c89de92467b13c2a69b8d60ff.jpg', '2020-03-09'),
(19, 'Oven', '10.000/12 jam/rak (pengeringan)', 0, 20000, 'Terawat', 2, 0, 0, 1, 'db59e02b0270182206230d7ff9c6f7d9.jpg', '2020-03-10'),
(20, 'Waterbath', '-', 0, 10000, 'Terawat', 2, 0, 0, 3, '53debde03d139937f807fcdd34308ca6.png', '2020-03-10'),
(21, 'Glukosa reader', 'Tdk termasuk kit', 0, 20000, 'Terawat', 1, 1, 3, 0, 'dafe86df337cd4d0d88b4c9547f7caf0.jpg', '2020-03-10'),
(22, 'Optilab microskop', '-', 0, 5000, 'Terawat', 1, 1, 6, 0, 'a0efe510a8259830e6d2be8f8ed9117b.jpg', '2020-03-10'),
(23, 'Rotary evaporator', '-', 0, 10000, 'Terawat', 1, 1, 6, 0, '8dfc20ec826f90db872d36084d68f301.jpg', '2020-03-10'),
(24, 'Autoclave', '-', 0, 10000, 'Terawat', 1, 1, 6, 0, '5253c54f58dfb645e2c53a24290f8c6e.jpg', '2020-03-10'),
(25, 'Soxlet', '-', 0, 3000, 'Terawat', 1, 1, 6, 0, 'e61cd2703e96832bc846856d8668796e.jpg', '2020-03-10'),
(26, 'Tanur', '-', 0, 40000, 'Terawat', 2, 0, 0, 3, '96e46d27f8142d8732af0e91f54d7a3b.jpg', '2020-03-10'),
(27, 'Disolution tester', '-', 0, 10000, 'Terawat', 1, 1, 6, 0, 'c4a3dcec317f8c747cada97298308a54.png', '2020-03-10'),
(28, 'Elisa reader', '-', 0, 50000, 'Terawat', 1, 1, 6, 0, 'ac68f0e05603790d242fd5369aceaf5f.jpg', '2020-03-10'),
(29, 'Kandang Tikus / Mencit', '-', 0, 3000, 'Terawat', 2, 0, 0, 30, 'f1341c8ebd5b0a56d2301526d144e168.png', '2020-03-11'),
(30, 'Halogen moisture analyzer', '-', 0, 10000, 'Terawat', 1, 1, 6, 0, '92b0ccf1eb907ff43f52f5a7d48c54e3.jpg', '2020-03-11'),
(31, 'TLC Scanner 4', '-', 0, 45000, 'Terawat', 1, 1, 6, 0, 'cd71fbe09b9e1059fa2b3e5e50246501.png', '2020-03-11'),
(32, 'Climatic Chamber', '-', 0, 15000, 'Terawat', 2, 0, 0, 30, '0b33566876c82168676867bf5fa79b59.jpg', '2020-03-11'),
(33, 'Dehumidifier', '-', 0, 5000, 'Terawat', 2, 1, 0, 7, '2d6631755bda04cf942414bec881de33.png', '2020-03-11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`barang_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `barang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
